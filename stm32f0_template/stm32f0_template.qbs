import qbs

Project
{
    property string board : "stm32f0discovery"
    property string mcu: "stm32f051"
    property string name: "stm32f0_template"
    CppApplication {
       type: "application"
       consoleApplication: true

       name : project.name
       cpp.executableSuffix: ".elf"
       cpp.positionIndependentCode: false

        Group {
            name : "stm32f0_lib"
            files : [
                "src/stm32f0lib/CMSIS/Device/ST/STM32F0xx/Include/stm32f0xx.h",
                "src/stm32f0lib/CMSIS/Device/ST/STM32F0xx/Include/system_stm32f0xx.h",
                "src/stm32f0lib/CMSIS/Include/arm_common_tables.h",
                "src/stm32f0lib/CMSIS/Include/arm_const_structs.h",
                "src/stm32f0lib/CMSIS/Include/arm_math.h",
                "src/stm32f0lib/CMSIS/Include/core_cm0.h",
                "src/stm32f0lib/CMSIS/Include/core_cmFunc.h",
                "src/stm32f0lib/CMSIS/Include/core_cmInstr.h",
                "src/stm32f0lib/CMSIS/Include/core_sc300.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_adc.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_can.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_cec.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_comp.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_crc.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_crs.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_dac.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_dbgmcu.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_dma.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_exti.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_flash.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_gpio.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_i2c.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_iwdg.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_misc.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_pwr.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_rcc.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_rtc.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_spi.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_syscfg.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_tim.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_usart.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc/stm32f0xx_wwdg.h",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_adc.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_can.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_cec.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_comp.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_crc.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_crs.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_dac.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_dbgmcu.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_dma.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_exti.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_flash.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_gpio.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_i2c.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_iwdg.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_misc.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_pwr.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_rcc.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_rtc.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_spi.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_syscfg.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_tim.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_usart.c",
                "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/src/stm32f0xx_wwdg.c",
            ]
        }

        Group {
            name: "mcu_concrete"
            files : [
                "src/interrupts.cpp",
                "src/stm32f051_startup.c",
                "src/stm32f0xx_conf.h",
                "src/system_stm32f0xx.c",
                "src/main.h",
            ]
        }

        Group {
            name : "app"
            files : ["src/main.cpp"]
        }

        Group {
            name : "x_other"
            files : [
                "README.md",
                "ldscripts/STM32F051R8_FLASH.ld",
            ]
        }

        property stringList MCU: ["-mthumb","-mcpu=cortex-m0"]
        property stringList FPU: []

        cpp.defines:{
            var defs = base
            defs = defs.concat(["USE_STDPERIPH_DRIVER","USE_FULL_ASSERT"])
            if(project.mcu == "stm32f051")
                defs = defs.concat(["STM32F051"])

            return defs
        }

        cpp.linkerScripts : ["ldscripts/STM32F051R8_FLASH.ld"]
        cpp.commonCompilerFlags: {
            var flags = base;
            flags = flags.concat(["-fdata-sections","-ffunction-sections","-flto","-fno-builtin","-nostdlib"])
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }
        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++11","-fno-exceptions","-fno-rtti"])
            return flags
        }
        cpp.cFlags:{
            var flags = base
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            flags = flags.concat(["--specs=nano.specs","--specs=rdimon.specs"])
            flags = flags.concat(["-Wl,--gc-sections","-lc", "-lgcc", "-lm", "-lrdimon"])
            flags = flags.concat(["-flto"])
            var map_name = project.buildDirectory+"/"+product.name+".map";
            flags = flags.concat(["-Wl,-Map="+map_name+",--cref"])

            return flags
        }

        cpp.includePaths : {
            var inc = base;
            inc = inc.concat(["src",
                              "src/stm32f0lib/CMSIS/Device/ST/STM32F0xx/Include/",
                              "src/stm32f0lib/CMSIS/Include",
                              "src/stm32f0lib/STM32F0xx_StdPeriph_Driver/inc"])

            return inc
        }

    }
}


