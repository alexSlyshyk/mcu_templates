
#include <cstdint>
#include <cstdio>
#include "stm32f0xx.h"


extern "C" void SysTick_Handler(void)
{
    static uint32_t current_tick = 0;

    if( (current_tick % 500) == 0)
        {
            if(GPIO_ReadOutputDataBit(GPIOC,GPIO_Pin_9))
                GPIO_WriteBit(GPIOC,GPIO_Pin_9,Bit_RESET);
            else
                GPIO_WriteBit(GPIOC,GPIO_Pin_9,Bit_SET);
        }

    current_tick++;
}


extern "C" void hard_fault_handler_c (unsigned int * hardfault_args)  __attribute__((used)) ;


/**
  * @brief   This function handles NMI exception.
  * @param  None
  * @retval None
  */
extern "C" void NMI_Handler(void)
{
    assert_param(false);
    hard_fault_handler_c(nullptr);
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
extern "C" void HardFault_Handler(void) //  __attribute__((naked))
{
//    __ASM("TST LR, #4");
//    __ASM("ITE EQ");
//    __ASM("MRSEQ R0, MSP");
//    __ASM("MRSNE R0, PSP");
    __ASM("B hard_fault_handler_c");

    /* Go to infinite loop when Hard Fault exception occurs */
    while (1)
    {
        assert_param(false);
    }
}

extern "C" void hard_fault_handler_c (unsigned int * hardfault_args)
{
    unsigned int stacked_r0;
    unsigned int stacked_r1;
    unsigned int stacked_r2;
    unsigned int stacked_r3;
    unsigned int stacked_r12;
    unsigned int stacked_lr;
    unsigned int stacked_pc;
    unsigned int stacked_psr;

    stacked_r0 = ((unsigned long) hardfault_args[0]);
    stacked_r1 = ((unsigned long) hardfault_args[1]);
    stacked_r2 = ((unsigned long) hardfault_args[2]);
    stacked_r3 = ((unsigned long) hardfault_args[3]);

    stacked_r12 = ((unsigned long) hardfault_args[4]);
    stacked_lr = ((unsigned long) hardfault_args[5]);
    stacked_pc = ((unsigned long) hardfault_args[6]);
    stacked_psr = ((unsigned long) hardfault_args[7]);

    printf ("\n\n[Hard fault handler - all numbers in hex]\n");
    printf ("R0 = %x\n", stacked_r0);
    printf ("R1 = %x\n", stacked_r1);
    printf ("R2 = %x\n", stacked_r2);
    printf ("R3 = %x\n", stacked_r3);
    printf ("R12 = %x\n", stacked_r12);
    printf ("LR [R14] = %x  subroutine call return address\n", stacked_lr);
    printf ("PC [R15] = %x  program counter\n", stacked_pc);
    printf ("PSR = %x\n", stacked_psr);
    printf ("BFAR = %lx\n", (*((volatile unsigned long *)(0xE000ED38))));
    printf ("CFSR = %lx\n", (*((volatile unsigned long *)(0xE000ED28))));
    printf ("HFSR = %lx\n", (*((volatile unsigned long *)(0xE000ED2C))));
    printf ("DFSR = %lx\n", (*((volatile unsigned long *)(0xE000ED30))));
    printf ("AFSR = %lx\n", (*((volatile unsigned long *)(0xE000ED3C))));
    printf ("SCB_SHCSR = %lx\n", SCB->SHCSR);

    while (1);
}


