

extern unsigned long _sidata;
extern unsigned long _sdata;
extern unsigned long _edata;
extern unsigned long _sbss;
extern unsigned long _ebss;
extern unsigned long _estack;
extern unsigned long __ctors_start__;
extern unsigned long __ctors_end__;

void Reset_Handler(void) __attribute__((__interrupt__));
extern int main(void);
extern void SystemInit(void);
extern void SystemCoreClockUpdate(void);
void __Init_Data(void);

/* Core interrupt vectors */
void  NMI_Handler (void);
void  HardFault_Handler (void)__attribute__((used));
void  SVC_Handler (void);
void  PendSV_Handler (void);
void  SysTick_Handler (void);

/*Device handler*/
void WWDG_IRQHandler (void);
void PVD_IRQHandler (void);
void RTC_IRQHandler (void);
void FLASH_IRQHandler (void);
void RCC_IRQHandler (void);
void EXTI0_1_IRQHandler (void);
void EXTI2_3_IRQHandler (void);
void EXTI4_15_IRQHandler (void);
void TS_IRQHandler (void);
void DMA1_Channel1_IRQHandler (void);
void DMA1_Channel2_3_IRQHandler (void);
void DMA1_Channel4_5_IRQHandler (void);
void ADC1_COMP_IRQHandler  (void);
void TIM1_BRK_UP_TRG_COM_IRQHandler (void);
void TIM1_CC_IRQHandler (void);
void TIM2_IRQHandler (void);
void TIM3_IRQHandler (void);
void TIM6_DAC_IRQHandler (void);

void TIM14_IRQHandler (void);
void TIM15_IRQHandler (void);
void TIM16_IRQHandler (void);
void TIM17_IRQHandler (void);
void I2C1_IRQHandler (void);
void I2C2_IRQHandler (void);
void SPI1_IRQHandler (void);
void SPI2_IRQHandler (void);
void USART1_IRQHandler (void);
void USART2_IRQHandler (void);

void CEC_IRQHandler (void);

/******************************************************************************
* Vector table for a Cortex M0.
******************************************************************************/
typedef void( *const intfunc )( void );

__attribute__ ((used))
__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) =
{
    /* Core interrupt vectors */
    (intfunc)((unsigned long)&_estack),
    Reset_Handler,

    NMI_Handler,
    HardFault_Handler,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    SVC_Handler,
    0,
    0,
    PendSV_Handler,
    SysTick_Handler,

    WWDG_IRQHandler,
    PVD_IRQHandler,
    RTC_IRQHandler,
    FLASH_IRQHandler,
    RCC_IRQHandler,
    EXTI0_1_IRQHandler,
    EXTI2_3_IRQHandler,
    EXTI4_15_IRQHandler,
    TS_IRQHandler,
    DMA1_Channel1_IRQHandler,
    DMA1_Channel2_3_IRQHandler,
    DMA1_Channel4_5_IRQHandler,
    ADC1_COMP_IRQHandler ,
    TIM1_BRK_UP_TRG_COM_IRQHandler,
    TIM1_CC_IRQHandler,
    TIM2_IRQHandler,
    TIM3_IRQHandler,
    TIM6_DAC_IRQHandler,
    0  ,
    TIM14_IRQHandler,
    TIM15_IRQHandler,
    TIM16_IRQHandler,
    TIM17_IRQHandler,
    I2C1_IRQHandler,
    I2C2_IRQHandler,
    SPI1_IRQHandler,
    SPI2_IRQHandler,
    USART1_IRQHandler,
    USART2_IRQHandler,
    0,
    CEC_IRQHandler,
    0
};


void Reset_Handler()
{
    __Init_Data();
    main();
}

extern void __libc_init_array(void);
void __Init_Data(void)
{
    unsigned long *pulSrc, *pulDest;

    pulSrc = &_sidata;

    for(pulDest = &_sdata; pulDest < &_edata; )
        *(pulDest++) = *(pulSrc++);

    for(pulDest = &_sbss; pulDest < &_ebss; )
        *(pulDest++) = 0;

    /* Init hardware before calling constructors */
    //init_HW();
    SystemInit();
    SystemCoreClockUpdate();


    /* Call constructors */
    unsigned long *ctors;
    for(ctors = &__ctors_start__; ctors < &__ctors_end__; )
        ((void(*)(void))(*ctors++))();
}

/*******************************************************************************
*
* Provide weak aliases for each Exception handler to the Default_Handler.
* As they are weak aliases, any function with the same name will override
* this definition.
*
*******************************************************************************/

#pragma weak NMI_Handler                    = Default_Handler
#pragma weak HardFault_Handler              = Default_Handler
#pragma weak SVC_Handler                    = Default_Handler
#pragma weak PendSV_Handler                 = Default_Handler
#pragma weak SysTick_Handler                = Default_Handler
#pragma weak WWDG_IRQHandler                = Default_Handler
#pragma weak PVD_IRQHandler                 = Default_Handler
#pragma weak RTC_IRQHandler                 = Default_Handler
#pragma weak FLASH_IRQHandler               = Default_Handler
#pragma weak RCC_IRQHandler                 = Default_Handler
#pragma weak EXTI0_1_IRQHandler             = Default_Handler
#pragma weak EXTI2_3_IRQHandler             = Default_Handler
#pragma weak EXTI4_15_IRQHandler            = Default_Handler
#pragma weak TS_IRQHandler                  = Default_Handler
#pragma weak DMA1_Channel1_IRQHandler       = Default_Handler
#pragma weak DMA1_Channel2_3_IRQHandler     = Default_Handler
#pragma weak DMA1_Channel4_5_IRQHandler     = Default_Handler
#pragma weak ADC1_COMP_IRQHandler           = Default_Handler
#pragma weak TIM1_BRK_UP_TRG_COM_IRQHandler = Default_Handler
#pragma weak TIM1_CC_IRQHandler             = Default_Handler
#pragma weak TIM2_IRQHandler                = Default_Handler
#pragma weak TIM3_IRQHandler                = Default_Handler
#pragma weak TIM6_DAC_IRQHandler            = Default_Handler
#pragma weak TIM14_IRQHandler               = Default_Handler
#pragma weak TIM15_IRQHandler               = Default_Handler
#pragma weak TIM16_IRQHandler               = Default_Handler
#pragma weak TIM17_IRQHandler               = Default_Handler
#pragma weak I2C1_IRQHandler                = Default_Handler
#pragma weak I2C2_IRQHandler                = Default_Handler
#pragma weak SPI1_IRQHandler                = Default_Handler
#pragma weak SPI2_IRQHandler                = Default_Handler
#pragma weak USART1_IRQHandler              = Default_Handler
#pragma weak USART2_IRQHandler              = Default_Handler
#pragma weak CEC_IRQHandler                 = Default_Handler



void Default_Handler(void)
{
    for (;;);
}
