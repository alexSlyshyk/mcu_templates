
#include "main.h"


int main()
{
    SysTick_Config (SystemCoreClock / 1000);  // 1000 Hz
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
    GPIO_InitTypeDef gpio;
    GPIO_StructInit(&gpio);
    gpio.GPIO_Mode = GPIO_Mode_OUT;
    gpio.GPIO_OType = GPIO_OType_PP;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_8;
    GPIO_Init(GPIOC, &gpio);

    for(;;)
        {
            GPIO_WriteBit(GPIOC, GPIO_Pin_8, Bit_SET);
        }

    return 0;
}


#ifdef  USE_FULL_ASSERT
#include <cstdio>
extern "C" void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert in %s at line %li", (const char*)file, line);
}

#endif
