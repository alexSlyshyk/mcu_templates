import qbs
import qbs.Probes

Project {
    property string name : "f3cube_gpio"
    property string board: "discovetyF334"
    property string mcu  : "STM32F334x8"
    property stringList cubeF3searchPaths:["~/projects/cpplibs/mcu/stm32/STM32Cube_FW_F3_V1.3.0",
        "~/projects/cpplibs/mcu/stm32/STM32Cube_FW_F3_V1.4.0"
    ]
    qbsSearchPaths : ["~/projects/qbs_custom_modules/"]
    CppApplication{

        cpp.positionIndependentCode: false

        type: "application"

        Depends {name:"hex"}

        targetName:{
            var tn = "f3cube_gpio"
            if(project.with_bootloader)
                tn = tn + "_bt"
            tn = tn + ".elf"
            return tn
        }
        hex.targetName : {return product.targetName}

        property stringList cubePathSearch:project.cubeF3searchPaths
            property stringList __searchPaths:{
                var res = [];
                var home
                var searchPaths = cubePathSearch
                if(qbs.hostOS.contains("linux"))
                    home = qbs.getEnv("HOME")
                else
                    home = "d:"

                for (var i = 0; i < searchPaths.length; ++i){
                    var s = ""
                    if (searchPaths[i].startsWith("~")){
                        s = searchPaths[i].replace("~", home)
                    }
                    else{
                        s = searchPaths[i]
                    }
                    res.push(s)
                }
                return res
            }
            Probes.PathProbe{
                id:path_of_stm32f3cube
                names:["stm32f3xx.h"]
                pathSuffixes:["", "Drivers/CMSIS/Device/ST/STM32F3xx/Include"]
                platformPaths:{
                    var res = []
                    res = res.concat(__searchPaths)
                    return res
                }
            }
            property string cubePath:{
                var pp = "/CUBE_PATH_NOT_FOUND"
                if(path_of_stm32f3cube.found){
                    pp = path_of_stm32f3cube.path
                    var ccc = pp.split("/")
                    var rrr = []
                    if(ccc.length > 6){
                        for (var i = 0; i < ccc.length - 6; ++i){
                            rrr.push(ccc[i])
                        }
                        pp = rrr.join("/")
                    }
                }
                return pp
            }



        Group{
            name:"app"
            files: [
                "src/main.c",
                "src/main.h",
                "src/stm32f3xx_hal_conf.h",
                "src/stm32f3xx_hal_msp.c",
                "src/stm32f3xx_it.c",
                "src/stm32f3xx_it.h",
                "src/system_stm32f3xx.c",
            ]
        }

        Group{
            name:"bsp"
            files:[
                "src/bsp/startup_stm32f334x8.S.xxx",
                "src/stm32f334x8/*",
            ]
        }

        Group{
            name:"periph"
            prefix: {
                var pref = cubePath + "/Drivers/STM32F3xx_HAL_Driver/"
                return pref
            }
            files:{
                var files = ["Inc/*.h"]
                files = files.concat(["Src/*.c"])
                return files
            }
        }



        Group {
            name : "x_other"
            files : [
                "*.ld",
            ]
        }

        cpp.includePaths:["src", cubePath+"/Drivers/BSP/STM32F3348-Discovery/",
            cubePath + "/Drivers/CMSIS/Include/",
            cubePath + "/Drivers/STM32F3xx_HAL_Driver/Inc",
            cubePath + "/Drivers/CMSIS/Device/ST/STM32F3xx/Include/",]

        cpp.linkerScripts: ["STM32F334R8_FLASH.ld" ]

        cpp.defines:[project.mcu, "__VFP_FP__", "__FPU_PRESENT=1","USE_HAL_DRIVER","USE_FULL_ASSERT","ARM_MATH_CM4"]

        cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections", "-flto"]

        property stringList MCU_FLAGS: ["-mthumb","-mcpu=cortex-m4"]
        property stringList FPU_FLAGS: ["-mfloat-abi=hard", "-mfpu=fpv4-sp-d16" ]

        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++14","-fno-exceptions","-fno-rtti"])
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            return flags
        }

        cpp.cFlags:{
            var flags = base.concat(["-std=gnu11"])
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU_FLAGS)
            flags = flags.concat(FPU_FLAGS)
            flags = flags.concat(["-Wl,--gc-sections",])
            flags = flags.concat(["-flto"])
            var map_name = project.buildDirectory+"/"+product.name+".map";
            flags = flags.concat(["-Wl,-Map="+map_name+",--cref"])
            flags = flags.concat(["-specs=nano.specs","-specs=rdimon.specs"])
            return flags
        }
    }
}
