extern unsigned long _sidata;
extern unsigned long _sdata;
extern unsigned long _edata;
extern unsigned long _sbss;
extern unsigned long _ebss;
extern unsigned long _estack;
extern unsigned long __ctors_start__;
extern unsigned long __ctors_end__;

#define BootRAM (0x0)

void Reset_Handler(void) __attribute__((__interrupt__));
void __Init_Data(void);

extern int  main(void);
extern void SystemInit(void);
extern void SystemCoreClockUpdate(void);

/*Interrupts headers*/
void Reset_Handler                  (void);
void NMI_Handler                    (void);
void HardFault_Handler              (void);
void MemManage_Handler              (void);
void BusFault_Handler               (void);
void UsageFault_Handler             (void);
void SVC_Handler                    (void);
void DebugMon_Handler               (void);
void PendSV_Handler                 (void);
void SysTick_Handler                (void);
void WWDG_IRQHandler                (void);
void PVD_IRQHandler                 (void);
void TAMP_STAMP_IRQHandler          (void);
void RTC_WKUP_IRQHandler            (void);
void FLASH_IRQHandler               (void);
void RCC_IRQHandler                 (void);
void EXTI0_IRQHandler               (void);
void EXTI1_IRQHandler               (void);
void EXTI2_IRQHandler               (void);
void EXTI3_IRQHandler               (void);
void EXTI4_IRQHandler               (void);
void DMA1_Stream0_IRQHandler        (void);
void DMA1_Stream1_IRQHandler        (void);
void DMA1_Stream2_IRQHandler        (void);
void DMA1_Stream3_IRQHandler        (void);
void DMA1_Stream4_IRQHandler        (void);
void DMA1_Stream5_IRQHandler        (void);
void DMA1_Stream6_IRQHandler        (void);
void ADC_IRQHandler                 (void);
void CAN1_TX_IRQHandler             (void);
void CAN1_RX0_IRQHandler            (void);
void CAN1_RX1_IRQHandler            (void);
void CAN1_SCE_IRQHandler            (void);
void EXTI9_5_IRQHandler             (void);
void TIM1_BRK_TIM9_IRQHandler       (void);
void TIM1_UP_TIM10_IRQHandler       (void);
void TIM1_TRG_COM_TIM11_IRQHandler  (void);
void TIM1_CC_IRQHandler             (void);
void TIM2_IRQHandler                (void);
void TIM3_IRQHandler                (void);
void TIM4_IRQHandler                (void);
void I2C1_EV_IRQHandler             (void);
void I2C1_ER_IRQHandler             (void);
void I2C2_EV_IRQHandler             (void);
void I2C2_ER_IRQHandler             (void);
void SPI1_IRQHandler                (void);
void SPI2_IRQHandler                (void);
void USART1_IRQHandler              (void);
void USART2_IRQHandler              (void);
void USART3_IRQHandler              (void);
void EXTI15_10_IRQHandler           (void);
void RTC_Alarm_IRQHandler           (void);
void OTG_FS_WKUP_IRQHandler         (void);
void TIM8_BRK_TIM12_IRQHandler      (void);
void TIM8_UP_TIM13_IRQHandler       (void);
void TIM8_TRG_COM_TIM14_IRQHandler  (void);
void TIM8_CC_IRQHandler             (void);
void DMA1_Stream7_IRQHandler        (void);
void FSMC_IRQHandler                (void);
void SDIO_IRQHandler                (void);
void TIM5_IRQHandler                (void);
void SPI3_IRQHandler                (void);
void UART4_IRQHandler               (void);
void UART5_IRQHandler               (void);
void TIM6_DAC_IRQHandler            (void);
void TIM7_IRQHandler                (void);
void DMA2_Stream0_IRQHandler        (void);
void DMA2_Stream1_IRQHandler        (void);
void DMA2_Stream2_IRQHandler        (void);
void DMA2_Stream3_IRQHandler        (void);
void DMA2_Stream4_IRQHandler        (void);
void ETH_IRQHandler                 (void);
void ETH_WKUP_IRQHandler            (void);
void CAN2_TX_IRQHandler             (void);
void CAN2_RX0_IRQHandler            (void);
void CAN2_RX1_IRQHandler            (void);
void CAN2_SCE_IRQHandler            (void);
void OTG_FS_IRQHandler              (void);
void DMA2_Stream5_IRQHandler        (void);
void DMA2_Stream6_IRQHandler        (void);
void DMA2_Stream7_IRQHandler        (void);
void USART6_IRQHandler              (void);
void I2C3_EV_IRQHandler             (void);
void I2C3_ER_IRQHandler             (void);
void OTG_HS_EP1_OUT_IRQHandler      (void);
void OTG_HS_EP1_IN_IRQHandler       (void);
void OTG_HS_WKUP_IRQHandler         (void);
void OTG_HS_IRQHandler              (void);
void DCMI_IRQHandler                (void);
void HASH_RNG_IRQHandler            (void);
void FPU_IRQHandler                 (void);


/******************************************************************************
*                              Vector table                                   *
******************************************************************************/
typedef void( *const intfunc )( void );

__attribute__ ((used))
__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) = {
    (intfunc)((unsigned long)&_estack),
/*Interrupts*/
    Reset_Handler,
    NMI_Handler,
    HardFault_Handler,
    MemManage_Handler,
    BusFault_Handler,
    UsageFault_Handler,
    0, 0, 0, 0,
    SVC_Handler,
    DebugMon_Handler,
    0,
    PendSV_Handler,
    SysTick_Handler,
    WWDG_IRQHandler,
    PVD_IRQHandler,
    TAMP_STAMP_IRQHandler,
    RTC_WKUP_IRQHandler,
    FLASH_IRQHandler,
    RCC_IRQHandler,
    EXTI0_IRQHandler,
    EXTI1_IRQHandler,
    EXTI2_IRQHandler,
    EXTI3_IRQHandler,
    EXTI4_IRQHandler,
    DMA1_Stream0_IRQHandler,
    DMA1_Stream1_IRQHandler,
    DMA1_Stream2_IRQHandler,
    DMA1_Stream3_IRQHandler,
    DMA1_Stream4_IRQHandler,
    DMA1_Stream5_IRQHandler,
    DMA1_Stream6_IRQHandler,
    ADC_IRQHandler,
    CAN1_TX_IRQHandler,
    CAN1_RX0_IRQHandler,
    CAN1_RX1_IRQHandler,
    CAN1_SCE_IRQHandler,
    EXTI9_5_IRQHandler,
    TIM1_BRK_TIM9_IRQHandler,
    TIM1_UP_TIM10_IRQHandler,
    TIM1_TRG_COM_TIM11_IRQHandler,
    TIM1_CC_IRQHandler,
    TIM2_IRQHandler,
    TIM3_IRQHandler,
    TIM4_IRQHandler,
    I2C1_EV_IRQHandler,
    I2C1_ER_IRQHandler,
    I2C2_EV_IRQHandler,
    I2C2_ER_IRQHandler,
    SPI1_IRQHandler,
    SPI2_IRQHandler,
    USART1_IRQHandler,
    USART2_IRQHandler,
    USART3_IRQHandler,
    EXTI15_10_IRQHandler,
    RTC_Alarm_IRQHandler,
    OTG_FS_WKUP_IRQHandler,
    TIM8_BRK_TIM12_IRQHandler,
    TIM8_UP_TIM13_IRQHandler,
    TIM8_TRG_COM_TIM14_IRQHandler,
    TIM8_CC_IRQHandler,
    DMA1_Stream7_IRQHandler,
    FSMC_IRQHandler,
    SDIO_IRQHandler,
    TIM5_IRQHandler,
    SPI3_IRQHandler,
    UART4_IRQHandler,
    UART5_IRQHandler,
    TIM6_DAC_IRQHandler,
    TIM7_IRQHandler,
    DMA2_Stream0_IRQHandler,
    DMA2_Stream1_IRQHandler,
    DMA2_Stream2_IRQHandler,
    DMA2_Stream3_IRQHandler,
    DMA2_Stream4_IRQHandler,
    ETH_IRQHandler,
    ETH_WKUP_IRQHandler,
    CAN2_TX_IRQHandler,
    CAN2_RX0_IRQHandler,
    CAN2_RX1_IRQHandler,
    CAN2_SCE_IRQHandler,
    OTG_FS_IRQHandler,
    DMA2_Stream5_IRQHandler,
    DMA2_Stream6_IRQHandler,
    DMA2_Stream7_IRQHandler,
    USART6_IRQHandler,
    I2C3_EV_IRQHandler,
    I2C3_ER_IRQHandler,
    OTG_HS_EP1_OUT_IRQHandler,
    OTG_HS_EP1_IN_IRQHandler,
    OTG_HS_WKUP_IRQHandler,
    OTG_HS_IRQHandler,
    DCMI_IRQHandler,
    0,
    HASH_RNG_IRQHandler,
    FPU_IRQHandler,

};

void Reset_Handler()
{
    __Init_Data();
    main();
}

void __Init_Data(void)
{
    unsigned long *pulSrc, *pulDest;
    pulSrc = &_sidata;
    for(pulDest = &_sdata; pulDest < &_edata; )
        *(pulDest++) = *(pulSrc++);
    for(pulDest = &_sbss; pulDest < &_ebss; )
        *(pulDest++) = 0;
    /* Init hardware before calling constructors */
    SystemInit();
    SystemCoreClockUpdate();
    /* Call constructors */
    unsigned long *ctors;
    for(ctors = &__ctors_start__; ctors < &__ctors_end__; )
        ((void(*)(void))(*ctors++))();
}

/*Weak aliases for each exception handler*/
#pragma weak Reset_Handler                  = DefaultHandler 
#pragma weak NMI_Handler                    = DefaultHandler 
#pragma weak HardFault_Handler              = DefaultHandler 
#pragma weak MemManage_Handler              = DefaultHandler 
#pragma weak BusFault_Handler               = DefaultHandler 
#pragma weak UsageFault_Handler             = DefaultHandler 
#pragma weak SVC_Handler                    = DefaultHandler 
#pragma weak DebugMon_Handler               = DefaultHandler 
#pragma weak PendSV_Handler                 = DefaultHandler 
#pragma weak SysTick_Handler                = DefaultHandler 
#pragma weak WWDG_IRQHandler                = DefaultHandler 
#pragma weak PVD_IRQHandler                 = DefaultHandler 
#pragma weak TAMP_STAMP_IRQHandler          = DefaultHandler 
#pragma weak RTC_WKUP_IRQHandler            = DefaultHandler 
#pragma weak FLASH_IRQHandler               = DefaultHandler 
#pragma weak RCC_IRQHandler                 = DefaultHandler 
#pragma weak EXTI0_IRQHandler               = DefaultHandler 
#pragma weak EXTI1_IRQHandler               = DefaultHandler 
#pragma weak EXTI2_IRQHandler               = DefaultHandler 
#pragma weak EXTI3_IRQHandler               = DefaultHandler 
#pragma weak EXTI4_IRQHandler               = DefaultHandler 
#pragma weak DMA1_Stream0_IRQHandler        = DefaultHandler 
#pragma weak DMA1_Stream1_IRQHandler        = DefaultHandler 
#pragma weak DMA1_Stream2_IRQHandler        = DefaultHandler 
#pragma weak DMA1_Stream3_IRQHandler        = DefaultHandler 
#pragma weak DMA1_Stream4_IRQHandler        = DefaultHandler 
#pragma weak DMA1_Stream5_IRQHandler        = DefaultHandler 
#pragma weak DMA1_Stream6_IRQHandler        = DefaultHandler 
#pragma weak ADC_IRQHandler                 = DefaultHandler 
#pragma weak CAN1_TX_IRQHandler             = DefaultHandler 
#pragma weak CAN1_RX0_IRQHandler            = DefaultHandler 
#pragma weak CAN1_RX1_IRQHandler            = DefaultHandler 
#pragma weak CAN1_SCE_IRQHandler            = DefaultHandler 
#pragma weak EXTI9_5_IRQHandler             = DefaultHandler 
#pragma weak TIM1_BRK_TIM9_IRQHandler       = DefaultHandler 
#pragma weak TIM1_UP_TIM10_IRQHandler       = DefaultHandler 
#pragma weak TIM1_TRG_COM_TIM11_IRQHandler  = DefaultHandler 
#pragma weak TIM1_CC_IRQHandler             = DefaultHandler 
#pragma weak TIM2_IRQHandler                = DefaultHandler 
#pragma weak TIM3_IRQHandler                = DefaultHandler 
#pragma weak TIM4_IRQHandler                = DefaultHandler 
#pragma weak I2C1_EV_IRQHandler             = DefaultHandler 
#pragma weak I2C1_ER_IRQHandler             = DefaultHandler 
#pragma weak I2C2_EV_IRQHandler             = DefaultHandler 
#pragma weak I2C2_ER_IRQHandler             = DefaultHandler 
#pragma weak SPI1_IRQHandler                = DefaultHandler 
#pragma weak SPI2_IRQHandler                = DefaultHandler 
#pragma weak USART1_IRQHandler              = DefaultHandler 
#pragma weak USART2_IRQHandler              = DefaultHandler 
#pragma weak USART3_IRQHandler              = DefaultHandler 
#pragma weak EXTI15_10_IRQHandler           = DefaultHandler 
#pragma weak RTC_Alarm_IRQHandler           = DefaultHandler 
#pragma weak OTG_FS_WKUP_IRQHandler         = DefaultHandler 
#pragma weak TIM8_BRK_TIM12_IRQHandler      = DefaultHandler 
#pragma weak TIM8_UP_TIM13_IRQHandler       = DefaultHandler 
#pragma weak TIM8_TRG_COM_TIM14_IRQHandler  = DefaultHandler 
#pragma weak TIM8_CC_IRQHandler             = DefaultHandler 
#pragma weak DMA1_Stream7_IRQHandler        = DefaultHandler 
#pragma weak FSMC_IRQHandler                = DefaultHandler 
#pragma weak SDIO_IRQHandler                = DefaultHandler 
#pragma weak TIM5_IRQHandler                = DefaultHandler 
#pragma weak SPI3_IRQHandler                = DefaultHandler 
#pragma weak UART4_IRQHandler               = DefaultHandler 
#pragma weak UART5_IRQHandler               = DefaultHandler 
#pragma weak TIM6_DAC_IRQHandler            = DefaultHandler 
#pragma weak TIM7_IRQHandler                = DefaultHandler 
#pragma weak DMA2_Stream0_IRQHandler        = DefaultHandler 
#pragma weak DMA2_Stream1_IRQHandler        = DefaultHandler 
#pragma weak DMA2_Stream2_IRQHandler        = DefaultHandler 
#pragma weak DMA2_Stream3_IRQHandler        = DefaultHandler 
#pragma weak DMA2_Stream4_IRQHandler        = DefaultHandler 
#pragma weak ETH_IRQHandler                 = DefaultHandler 
#pragma weak ETH_WKUP_IRQHandler            = DefaultHandler 
#pragma weak CAN2_TX_IRQHandler             = DefaultHandler 
#pragma weak CAN2_RX0_IRQHandler            = DefaultHandler 
#pragma weak CAN2_RX1_IRQHandler            = DefaultHandler 
#pragma weak CAN2_SCE_IRQHandler            = DefaultHandler 
#pragma weak OTG_FS_IRQHandler              = DefaultHandler 
#pragma weak DMA2_Stream5_IRQHandler        = DefaultHandler 
#pragma weak DMA2_Stream6_IRQHandler        = DefaultHandler 
#pragma weak DMA2_Stream7_IRQHandler        = DefaultHandler 
#pragma weak USART6_IRQHandler              = DefaultHandler 
#pragma weak I2C3_EV_IRQHandler             = DefaultHandler 
#pragma weak I2C3_ER_IRQHandler             = DefaultHandler 
#pragma weak OTG_HS_EP1_OUT_IRQHandler      = DefaultHandler 
#pragma weak OTG_HS_EP1_IN_IRQHandler       = DefaultHandler 
#pragma weak OTG_HS_WKUP_IRQHandler         = DefaultHandler 
#pragma weak OTG_HS_IRQHandler              = DefaultHandler 
#pragma weak DCMI_IRQHandler                = DefaultHandler 
#pragma weak HASH_RNG_IRQHandler            = DefaultHandler 
#pragma weak FPU_IRQHandler                 = DefaultHandler 

void Default_Handler(void){  for (;;);}
