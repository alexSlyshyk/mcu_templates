@call set_vars.bat

%GNU_TOOLS_DIR%/bin/arm-none-eabi-objcopy.exe -O ihex   ../qtc_%QT_SDK%-release/%TARGET_NAME%.qtc_%QT_SDK%/%TARGET_NAME%.elf   ../exe/%TARGET_NAME%.hex
%GNU_TOOLS_DIR%/bin/arm-none-eabi-objcopy.exe -O binary ../qtc_%QT_SDK%-release/%TARGET_NAME%.qtc_%QT_SDK%/%TARGET_NAME%.elf   ../exe/%TARGET_NAME%.bin
%GNU_TOOLS_DIR%/bin/arm-none-eabi-objdump.exe -dC       ../qtc_%QT_SDK%-release/%TARGET_NAME%.qtc_%QT_SDK%/%TARGET_NAME%.elf > ../lst/%TARGET_NAME%.lss

