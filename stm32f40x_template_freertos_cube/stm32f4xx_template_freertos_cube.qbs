import qbs

Project
{
    property string board : "stm32f4discovery"
    property string mcu: "STM32F407VG"
    property string name: "stm32f40x_template_freertos_cube"
    CppApplication {
       type: "application"
       consoleApplication: true

       name : project.name
       cpp.executableSuffix: ".elf"
       cpp.positionIndependentCode: false

        Group {
            name : "stm32f4xxlib"
            files : [
                "src/stm32f40xlib/Drivers/CMSIS/Device/ST/STM32F4xx/Include/stm32f4xx.h",
                "src/stm32f40xlib/Drivers/CMSIS/Device/ST/STM32F4xx/Include/system_stm32f4xx.h",
                "src/stm32f40xlib/Drivers/CMSIS/Include/core_cm4.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_adc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_adc_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_can.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_conf_template.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_cortex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_crc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_cryp.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_cryp_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dac.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dac_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dcmi.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_def.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dma.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dma2d.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dma_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_eth.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_flash.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_flash_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_flash_ramfunc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_gpio.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_gpio_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_hash.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_hash_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_hcd.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_i2c.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_i2c_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_i2s.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_i2s_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_irda.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_iwdg.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_ltdc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_nand.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_nor.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pccard.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pcd.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pcd_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pwr.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pwr_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rcc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rcc_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rng.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rtc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rtc_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_sai.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_sd.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_sdram.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_smartcard.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_spi.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_sram.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_tim.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_tim_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_uart.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_usart.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_wwdg.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_fmc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_fsmc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_sdmmc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_usb.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_can.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_crc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma2d.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_eth.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hcd.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_irda.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_iwdg.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nand.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nor.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pccard.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sd.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_smartcard.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sram.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_usart.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_wwdg.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fsmc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_sdmmc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c",
            ]
        }

        Group {
            name: "stm32f4xx"
            files : [
                "src/stm32f40x/main.h",
                "src/stm32f40x/startups/startup_stm32f401xc.c",
                "src/stm32f40x/stm32f4xx_hal_conf.h",
                "src/stm32f40x/stm32f4xx_hal_msp.c",
                "src/stm32f40x/stm32f4xx_it.c",
                "src/stm32f40x/stm32f4xx_it.h",
                "src/stm32f40x/system_stm32f4xx.c",
            ]
        }

        Group {
            files: [
                "src/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c",
                "src/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.h",
                "src/FreeRTOS/Source/croutine.c",
                "src/FreeRTOS/Source/event_groups.c",
                "src/FreeRTOS/Source/include/FreeRTOS.h",
                "src/FreeRTOS/Source/include/FreeRTOSConfig.h",
                "src/FreeRTOS/Source/include/StackMacros.h",
                "src/FreeRTOS/Source/include/croutine.h",
                "src/FreeRTOS/Source/include/event_groups.h",
                "src/FreeRTOS/Source/include/list.h",
                "src/FreeRTOS/Source/include/mpu_wrappers.h",
                "src/FreeRTOS/Source/include/portable.h",
                "src/FreeRTOS/Source/include/projdefs.h",
                "src/FreeRTOS/Source/include/queue.h",
                "src/FreeRTOS/Source/include/semphr.h",
                "src/FreeRTOS/Source/include/stdint.readme",
                "src/FreeRTOS/Source/include/task.h",
                "src/FreeRTOS/Source/include/timers.h",
                "src/FreeRTOS/Source/list.c",
                "src/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c",
                "src/FreeRTOS/Source/portable/GCC/ARM_CM4F/portmacro.h",
                "src/FreeRTOS/Source/portable/MemMang/heap_4.c",
                "src/FreeRTOS/Source/queue.c",
                "src/FreeRTOS/Source/tasks.c",
                "src/FreeRTOS/Source/timers.c",
            ]
            name : "freeRTOS"
        }

        Group {
            name : "app"
            files : [
                "src/app.cpp",
                "src/app.hpp",
                "src/ledtask.cpp",
                "src/ledtask.hpp",
                "src/main.cpp",
                "src/tasks.cpp",
                "src/tasks.hpp",
            ]
        }

        Group {
            name : "board"
            prefix : "src/board/"
            files: [
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery.c",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery.h",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery_accelerometer.c",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery_accelerometer.h",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery_gyroscope.c",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery_gyroscope.h",
            ]
        }


        Group {
            name : "x_other"
            files : [
                "README.md",
                "ldscripts/STM32F401CC_FLASH.ld",
                "ldscripts/STM32F407VG_FLASH.ld",
            ]
        }

        property stringList MCU: ["-mthumb","-mcpu=cortex-m4","-mfloat-abi=hard"]
        property stringList FPU: ["-mfpu=fpv4-sp-d16"]

        cpp.defines:{
            var defs = base
            defs = defs.concat(["USE_STDPERIPH_DRIVER","USE_FULL_ASSERT"])
            if(project.mcu == "STM32F401VC")
                defs = defs.concat(["STM32F401xC"])
            if(project.mcu == "STM32F407VG")
                defs = defs.concat(["STM32F407xx"])

            return defs
        }

        cpp.linkerScripts : ["ldscripts/STM32F407VG_FLASH.ld"]

        cpp.commonCompilerFlags: {
            var flags = base;
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            flags = flags.concat(["-fdata-sections","-ffunction-sections","-flto"])

            return flags
        }

        cpp.cxxFlags:{
            var flags = base
            if(cpp.compilerName.contains("g++"))
                flags = flags.concat(["-std=gnu++11","-fno-exceptions","-fno-rtti"])
            return flags
        }

        cpp.cFlags:{
            var flags = base
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            flags = flags.concat(["-Wl,--gc-sections","-lc", "-lgcc","-lnosys"])
            flags = flags.concat(["-flto"])
            flags = flags.concat(["-Wl,-Map="+path+"/lst/"+project.name+".map,--cref"])
            return flags
        }

        cpp.includePaths : {
            var inc = base;
            inc = inc.concat(["src",
                              "src/stm32f40x",
                              "src/stm32f40xlib/Drivers/BSP/STM32F401-Discovery/",
                              "src/stm32f40xlib/Drivers/CMSIS/Include",
                              "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc",
                              "src/stm32f40xlib/Drivers/CMSIS/Device/ST/STM32F4xx/Include",
                              "src/FreeRTOS/Source/include",
                              "src/FreeRTOS/Source/portable/GCC/ARM_CM4F",
                              "src/FreeRTOS/Source/CMSIS_RTOS",
                             ])

            return inc
        }

    }
}


