import qbs

Project
{
    property string board : "stm32f0discovery"
    property string mcu: "stm32f100RB"
    property string name: "stm32f100_template"
    CppApplication {
       type: "application"
       consoleApplication: true

       name : project.name
       cpp.executableSuffix: ".elf"
       cpp.positionIndependentCode: false

        Group {
            name : "stm32f100_lib"
            files : [
                "src/stm32f100lib/Drivers/CMSIS/Device/ST/STM32F1xx/Include/stm32f1xx.h",
                "src/stm32f100lib/Drivers/CMSIS/Device/ST/STM32F1xx/Include/system_stm32f1xx.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_adc.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_adc_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_can.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_can_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_cec.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_cortex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_crc.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_dac.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_dac_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_def.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_dma.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_dma_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_eth.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_flash.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_flash_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_gpio.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_gpio_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_hcd.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_i2c.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_i2s.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_irda.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_iwdg.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_nand.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_nor.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_pccard.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_pcd.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_pcd_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_pwr.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_rcc.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_rcc_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_rtc.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_rtc_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_sd.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_smartcard.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_spi.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_sram.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_tim.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_tim_ex.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_uart.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_usart.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_hal_wwdg.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_ll_fsmc.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_ll_sdmmc.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc/stm32f1xx_ll_usb.h",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_adc.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_adc_ex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_can.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cec.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_crc.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dac.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dac_ex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_eth.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio_ex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_hcd.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_i2c.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_i2s.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_irda.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_iwdg.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_nand.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_nor.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pccard.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pcd.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pcd_ex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rtc.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rtc_ex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_sd.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_smartcard.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_spi.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_spi_ex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_sram.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_uart.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_usart.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_wwdg.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_ll_fsmc.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_ll_sdmmc.c",
                "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_ll_usb.c",
            ]
        }

        Group {
            name: "stm32f100"
            files : [
                "src/stm32f100/main.h",
                "src/stm32f100/startup_stm32f100_ld_vl.c",
                "src/stm32f100/startup_stm32f100_md_vl.c",
                "src/stm32f100/stm32f1xx_hal_conf.h",
                "src/stm32f100/stm32f1xx_hal_msp.c",
                "src/stm32f100/stm32f1xx_it.c",
                "src/stm32f100/stm32f1xx_it.h",
                "src/stm32f100/system_stm32f1xx.c",
            ]
        }

        Group {
            name : "app"
            files : [
                "src/main.cpp",
            ]
        }

        Group {
            name : "board"
            prefix : "src/board/"
            files: [
                "../stm32f100lib/Drivers/BSP/STM32VL-Discovery/stm32vl_discovery.c",
                "../stm32f100lib/Drivers/BSP/STM32VL-Discovery/stm32vl_discovery.h",
            ]
        }


        Group {
            name : "x_other"
            files : [
                "README.md",
                "linker/STM32F100XB_FLASH.ld",
            ]
        }

        property stringList MCU: ["-mthumb","-mcpu=cortex-m3"]
        property stringList FPU: []

        cpp.defines:{
            var defs = base
            defs = defs.concat(["USE_STDPERIPH_DRIVER","USE_FULL_ASSERT"])
            if(project.mcu == "stm32f100RB")
                defs = defs.concat(["STM32F100xB"])

            return defs
        }

        cpp.linkerScripts : ["linker/STM32F100XB_FLASH.ld"]
        cpp.commonCompilerFlags: {
            var flags = base;
            flags = flags.concat(["-g","-fdata-sections","-ffunction-sections","-flto","-fno-builtin","-nostdlib","-nostartfiles"])
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }
        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++11","-fno-exceptions","-fno-rtti","-fno-threadsafe-statics"])
            return flags
        }
        cpp.cFlags:{
            var flags = base
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            flags = flags.concat(["--specs=nano.specs","--specs=rdimon.specs"])
            flags = flags.concat(["-Wl,--gc-sections","-nostdlib", "-nostartfiles"])
            flags = flags.concat(["-lc"])
            flags = flags.concat(["-lgcc"])
            flags = flags.concat(["-lm"])
            flags = flags.concat(["-lrdimon"])
            flags = flags.concat(["-flto"])
            var map_name = project.buildDirectory+"/"+product.name+".map";
            flags = flags.concat(["-Wl,-Map="+map_name+",--cref"])

            return flags
        }

        cpp.includePaths : {
            var inc = base;
            inc = inc.concat(["src",
                              "src/stm32f100",
                              "src/stm32f100lib/Drivers/BSP/STM32VL-Discovery",
                              "src/stm32f100lib/Drivers/CMSIS/Include",
                              "src/stm32f100lib/Drivers/STM32F1xx_HAL_Driver/Inc",
                              "src/stm32f100lib/Drivers/CMSIS/Device/ST/STM32F1xx/Include",
                             ])

            return inc
        }

    }
}


