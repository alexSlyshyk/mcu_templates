extern unsigned long _sidata;
extern unsigned long _sdata;
extern unsigned long _edata;
extern unsigned long _sbss;
extern unsigned long _ebss;
extern unsigned long _estack;
extern unsigned long __ctors_start__;
extern unsigned long __ctors_end__;

#define BootRAM (0x0)

void Reset_Handler  (void) __attribute__((__interrupt__));
void Default_Handler(void);

extern int  main(void);
extern void SystemInit(void);
extern void SystemCoreClockUpdate(void);

/*Interrupts headers*/
void Reset_Handler                  (void);
void NMI_Handler                    (void);
void HardFault_Handler              (void);
void MemManage_Handler              (void);
void BusFault_Handler               (void);
void UsageFault_Handler             (void);
void SVC_Handler                    (void);
void DebugMon_Handler               (void);
void PendSV_Handler                 (void);
void SysTick_Handler                (void);
void WWDG_IRQHandler                (void);
void PVD_IRQHandler                 (void);
void TAMP_STAMP_IRQHandler          (void);
void RTC_WKUP_IRQHandler            (void);
void FLASH_IRQHandler               (void);
void RCC_IRQHandler                 (void);
void EXTI0_IRQHandler               (void);
void EXTI1_IRQHandler               (void);
void EXTI2_IRQHandler               (void);
void EXTI3_IRQHandler               (void);
void EXTI4_IRQHandler               (void);
void DMA1_Stream0_IRQHandler        (void);
void DMA1_Stream1_IRQHandler        (void);
void DMA1_Stream2_IRQHandler        (void);
void DMA1_Stream3_IRQHandler        (void);
void DMA1_Stream4_IRQHandler        (void);
void DMA1_Stream5_IRQHandler        (void);
void DMA1_Stream6_IRQHandler        (void);
void ADC_IRQHandler                 (void);
void EXTI9_5_IRQHandler             (void);
void TIM1_BRK_TIM9_IRQHandler       (void);
void TIM1_UP_TIM10_IRQHandler       (void);
void TIM1_TRG_COM_TIM11_IRQHandler  (void);
void TIM1_CC_IRQHandler             (void);
void TIM2_IRQHandler                (void);
void TIM3_IRQHandler                (void);
void TIM4_IRQHandler                (void);
void I2C1_EV_IRQHandler             (void);
void I2C1_ER_IRQHandler             (void);
void I2C2_EV_IRQHandler             (void);
void I2C2_ER_IRQHandler             (void);
void SPI1_IRQHandler                (void);
void SPI2_IRQHandler                (void);
void USART1_IRQHandler              (void);
void USART2_IRQHandler              (void);
void EXTI15_10_IRQHandler           (void);
void RTC_Alarm_IRQHandler           (void);
void OTG_FS_WKUP_IRQHandler         (void);
void DMA1_Stream7_IRQHandler        (void);
void SDIO_IRQHandler                (void);
void TIM5_IRQHandler                (void);
void SPI3_IRQHandler                (void);
void DMA2_Stream0_IRQHandler        (void);
void DMA2_Stream1_IRQHandler        (void);
void DMA2_Stream2_IRQHandler        (void);
void DMA2_Stream3_IRQHandler        (void);
void DMA2_Stream4_IRQHandler        (void);
void OTG_FS_IRQHandler              (void);
void DMA2_Stream5_IRQHandler        (void);
void DMA2_Stream6_IRQHandler        (void);
void DMA2_Stream7_IRQHandler        (void);
void USART6_IRQHandler              (void);
void I2C3_EV_IRQHandler             (void);
void I2C3_ER_IRQHandler             (void);
void FPU_IRQHandler                 (void);
void SPI4_IRQHandler                (void);


/******************************************************************************
*                              Vector table                                   *
******************************************************************************/
typedef void( *const intfunc )( void );

__attribute__ ((used))
__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) = {
    (intfunc)((unsigned long)&_estack),
/*Interrupts*/
    Reset_Handler,
    NMI_Handler,
    HardFault_Handler,
    MemManage_Handler,
    BusFault_Handler,
    UsageFault_Handler,
    0, 0, 0, 0,
    SVC_Handler,
    DebugMon_Handler,
    0,
    PendSV_Handler,
    SysTick_Handler,
    WWDG_IRQHandler,
    PVD_IRQHandler,
    TAMP_STAMP_IRQHandler,
    RTC_WKUP_IRQHandler,
    FLASH_IRQHandler,
    RCC_IRQHandler,
    EXTI0_IRQHandler,
    EXTI1_IRQHandler,
    EXTI2_IRQHandler,
    EXTI3_IRQHandler,
    EXTI4_IRQHandler,
    DMA1_Stream0_IRQHandler,
    DMA1_Stream1_IRQHandler,
    DMA1_Stream2_IRQHandler,
    DMA1_Stream3_IRQHandler,
    DMA1_Stream4_IRQHandler,
    DMA1_Stream5_IRQHandler,
    DMA1_Stream6_IRQHandler,
    ADC_IRQHandler,
    0, 0, 0, 0,
    EXTI9_5_IRQHandler,
    TIM1_BRK_TIM9_IRQHandler,
    TIM1_UP_TIM10_IRQHandler,
    TIM1_TRG_COM_TIM11_IRQHandler,
    TIM1_CC_IRQHandler,
    TIM2_IRQHandler,
    TIM3_IRQHandler,
    TIM4_IRQHandler,
    I2C1_EV_IRQHandler,
    I2C1_ER_IRQHandler,
    I2C2_EV_IRQHandler,
    I2C2_ER_IRQHandler,
    SPI1_IRQHandler,
    SPI2_IRQHandler,
    USART1_IRQHandler,
    USART2_IRQHandler,
    0,
    EXTI15_10_IRQHandler,
    RTC_Alarm_IRQHandler,
    OTG_FS_WKUP_IRQHandler,
    0, 0, 0, 0,
    DMA1_Stream7_IRQHandler,
    0,
    SDIO_IRQHandler,
    TIM5_IRQHandler,
    SPI3_IRQHandler,
    0, 0, 0, 0,
    DMA2_Stream0_IRQHandler,
    DMA2_Stream1_IRQHandler,
    DMA2_Stream2_IRQHandler,
    DMA2_Stream3_IRQHandler,
    DMA2_Stream4_IRQHandler,
    0, 0, 0, 0, 0, 0,
    OTG_FS_IRQHandler,
    DMA2_Stream5_IRQHandler,
    DMA2_Stream6_IRQHandler,
    DMA2_Stream7_IRQHandler,
    USART6_IRQHandler,
    I2C3_EV_IRQHandler,
    I2C3_ER_IRQHandler,
    0, 0, 0, 0, 0, 0, 0,
    FPU_IRQHandler,
    0, 0,
    SPI4_IRQHandler,

};

void  __libc_init_array (void);
void __Init_Data(void)
{
    unsigned long *pulSrc, *pulDest;
    pulSrc = &_sidata;
    for(pulDest = &_sdata; pulDest < &_edata; )
        *(pulDest++) = *(pulSrc++);
    for(pulDest = &_sbss; pulDest < &_ebss; )
        *(pulDest++) = 0;
    /* Init hardware before calling constructors */
    SystemInit();
    SystemCoreClockUpdate();
    /* Call constructors */
    __libc_init_array ();
//    unsigned long *ctors;
//    for(ctors = &__ctors_start__; ctors < &__ctors_end__; )
//        ((void(*)(void))(*ctors++))();
}

void Reset_Handler()
{
    __Init_Data();
    main();
}

/*Weak aliases for each exception handler*/
#pragma weak NMI_Handler                    = Default_Handler 
#pragma weak HardFault_Handler              = Default_Handler 
#pragma weak MemManage_Handler              = Default_Handler 
#pragma weak BusFault_Handler               = Default_Handler 
#pragma weak UsageFault_Handler             = Default_Handler 
#pragma weak SVC_Handler                    = Default_Handler 
#pragma weak DebugMon_Handler               = Default_Handler 
#pragma weak PendSV_Handler                 = Default_Handler 
#pragma weak SysTick_Handler                = Default_Handler 
#pragma weak WWDG_IRQHandler                = Default_Handler 
#pragma weak PVD_IRQHandler                 = Default_Handler 
#pragma weak TAMP_STAMP_IRQHandler          = Default_Handler 
#pragma weak RTC_WKUP_IRQHandler            = Default_Handler 
#pragma weak FLASH_IRQHandler               = Default_Handler 
#pragma weak RCC_IRQHandler                 = Default_Handler 
#pragma weak EXTI0_IRQHandler               = Default_Handler 
#pragma weak EXTI1_IRQHandler               = Default_Handler 
#pragma weak EXTI2_IRQHandler               = Default_Handler 
#pragma weak EXTI3_IRQHandler               = Default_Handler 
#pragma weak EXTI4_IRQHandler               = Default_Handler 
#pragma weak DMA1_Stream0_IRQHandler        = Default_Handler 
#pragma weak DMA1_Stream1_IRQHandler        = Default_Handler 
#pragma weak DMA1_Stream2_IRQHandler        = Default_Handler 
#pragma weak DMA1_Stream3_IRQHandler        = Default_Handler 
#pragma weak DMA1_Stream4_IRQHandler        = Default_Handler 
#pragma weak DMA1_Stream5_IRQHandler        = Default_Handler 
#pragma weak DMA1_Stream6_IRQHandler        = Default_Handler 
#pragma weak ADC_IRQHandler                 = Default_Handler 
#pragma weak EXTI9_5_IRQHandler             = Default_Handler 
#pragma weak TIM1_BRK_TIM9_IRQHandler       = Default_Handler 
#pragma weak TIM1_UP_TIM10_IRQHandler       = Default_Handler 
#pragma weak TIM1_TRG_COM_TIM11_IRQHandler  = Default_Handler 
#pragma weak TIM1_CC_IRQHandler             = Default_Handler 
#pragma weak TIM2_IRQHandler                = Default_Handler 
#pragma weak TIM3_IRQHandler                = Default_Handler 
#pragma weak TIM4_IRQHandler                = Default_Handler 
#pragma weak I2C1_EV_IRQHandler             = Default_Handler 
#pragma weak I2C1_ER_IRQHandler             = Default_Handler 
#pragma weak I2C2_EV_IRQHandler             = Default_Handler 
#pragma weak I2C2_ER_IRQHandler             = Default_Handler 
#pragma weak SPI1_IRQHandler                = Default_Handler 
#pragma weak SPI2_IRQHandler                = Default_Handler 
#pragma weak USART1_IRQHandler              = Default_Handler 
#pragma weak USART2_IRQHandler              = Default_Handler 
#pragma weak EXTI15_10_IRQHandler           = Default_Handler 
#pragma weak RTC_Alarm_IRQHandler           = Default_Handler 
#pragma weak OTG_FS_WKUP_IRQHandler         = Default_Handler 
#pragma weak DMA1_Stream7_IRQHandler        = Default_Handler 
#pragma weak SDIO_IRQHandler                = Default_Handler 
#pragma weak TIM5_IRQHandler                = Default_Handler 
#pragma weak SPI3_IRQHandler                = Default_Handler 
#pragma weak DMA2_Stream0_IRQHandler        = Default_Handler 
#pragma weak DMA2_Stream1_IRQHandler        = Default_Handler 
#pragma weak DMA2_Stream2_IRQHandler        = Default_Handler 
#pragma weak DMA2_Stream3_IRQHandler        = Default_Handler 
#pragma weak DMA2_Stream4_IRQHandler        = Default_Handler 
#pragma weak OTG_FS_IRQHandler              = Default_Handler 
#pragma weak DMA2_Stream5_IRQHandler        = Default_Handler 
#pragma weak DMA2_Stream6_IRQHandler        = Default_Handler 
#pragma weak DMA2_Stream7_IRQHandler        = Default_Handler 
#pragma weak USART6_IRQHandler              = Default_Handler 
#pragma weak I2C3_EV_IRQHandler             = Default_Handler 
#pragma weak I2C3_ER_IRQHandler             = Default_Handler 
#pragma weak FPU_IRQHandler                 = Default_Handler 
#pragma weak SPI4_IRQHandler                = Default_Handler 

void Default_Handler(void){  for (;;);}
