import qbs
import qbs.Probes

Project {
    property string name : "f3cube"
    property string board: "nucleoF334"
    property string mcu  : "STM32F334x8"
    property stringList cubeF3searchPaths:["~/projects/cpplibs/mcu/stm32/STM32Cube_FW_F3_V1.3.0",
        "~/projects/cpplibs/mcu/stm32/STM32Cube_FW_F3_V1.4.0"
    ]
    qbsSearchPaths : ["~/projects/qbs_custom_modules/"]
    CppApplication{

        cpp.positionIndependentCode: false

        type: "application"

        Depends {name:"hex"}
        Depends {name:"stm32f3cubelib" ; submodules:[project.mcu]}


        targetName:{
            var tn = "app"
            if(project.with_bootloader)
                tn = tn + "_bt"
            tn = tn + ".elf"
            return tn
        }
        hex.targetName : {return product.targetName}

        property stringList cubePathSearch:project.cubeF3searchPaths
            property stringList __searchPaths:{
                var res = [];
                var home
                var searchPaths = cubePathSearch
                if(qbs.hostOS.contains("linux"))
                    home = qbs.getEnv("HOME")
                else
                    home = "d:"

                for (var i = 0; i < searchPaths.length; ++i){
                    var s = ""
                    if (searchPaths[i].startsWith("~")){
                        s = searchPaths[i].replace("~", home)
                    }
                    else{
                        s = searchPaths[i]
                    }
                    res.push(s)
                }
                return res
            }
            Probes.PathProbe{
                id:path_of_stm32f3cube
                names:["stm32f3xx.h"]
                pathSuffixes:["", "Drivers/CMSIS/Device/ST/STM32F3xx/Include"]
                platformPaths:{
                    var res = []
                    res = res.concat(__searchPaths)
                    return res
                }
            }
            property string cubePath:{
                var pp = "/NO_WAY"
                if(path_of_stm32f3cube.found){
                    pp = path_of_stm32f3cube.path
                    var ccc = pp.split("/")
                    var rrr = []
                    if(ccc.length > 6){
                        for (var i = 0; i < ccc.length - 6; ++i){
                            rrr.push(ccc[i])
                        }
                        pp = rrr.join("/")
                    }
                }
                return pp
            }


        Group{
            name:"bsp"
            files:[
                "src/bsp/stm32f3xx_hal_msp.c",
            ]
        }
        Group{
            name:"discovery"
            prefix:cubePath+"/Drivers/BSP/STM32F3348-Discovery/"
            files:["*.h", "*.c"]
        }

        Group{
            name:"stm32f334x8"
            files:[
                "src/stm32f334x8/startup_stm32f334x8.c",
                "src/stm32f334x8/stm32f3xx_it.c",
                "src/stm32f334x8/stm32f3xx_it.h",
                "src/stm32f334x8/system_stm32f3xx.c",
            ]
        }
        Group{
            name:"app"
            files: [
                "src/main.c",
                "src/main.h",
            ]
        }

        Group {
            name : "x_other"
            files : [
                "*.ld",
            ]
        }

        cpp.includePaths:["src", cubePath+"/Drivers/BSP/STM32F3348-Discovery/"]
        cpp.linkerScripts: ["STM32F334R8_FLASH.ld" ]
        cpp.defines:["__VFP_FP__"]

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(["-Wl,--gc-sections",])
            flags = flags.concat(["-flto"])
            var map_name = project.buildDirectory+"/"+product.name+".map";
            flags = flags.concat(["-Wl,-Map="+map_name+",--cref"])
            return flags
        }
    }
}
