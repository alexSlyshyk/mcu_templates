import qbs
import qbs.FileInfo

Project {
    property string name : "stm32F4_tst_boot"
    property string board: "stm32F4discovery"
    qbsSearchPaths: ["."]
    CppApplication {
        Depends {name:"hex"}
        name:project.name
        cpp.executableSuffix: ".elf"
        cpp.positionIndependentCode: false

        type: "application"

        Group{
            name:"main"
            prefix:"src/"
            files:["*.h","*.c","*.cpp","*.hpp"]
        }
        Group{
            name:"board"
            prefix:"src/board/"
            files:["board.cpp","board.hpp",
                "brd_F4_discovery/brd_f4_discovery.hpp",
                "brd_F4_discovery/brd_f4_discovery.cpp",
                "brd_F4_discovery/stm32f4_discovery.h",
                "brd_F4_discovery/stm32f4_discovery.c"]
        }

        Group{
            name:"stm32f4"
            prefix:"src/stm32f4xx/"
            files:["*.h","*.c","*.cpp","*.hpp"]
        }
        Group{
            name:"stm32f4/drivers"
            prefix:"src/stm32f4xx/drivers/"
            files:["inc/*.h","src/*.c"]
        }
        Group{
            name:"CMSIS"
            prefix:"src/CMSIS/"
            files:["*.h"]
        }
        Group{
            name:"utils"
            prefix:"src/utils/"
            files:["*.h","*.c","*.cpp","*.hpp"]
        }

        cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections","-fno-inline","-nostdlib"]

        property stringList MCU: ["-mcpu=cortex-m4","-mthumb"]
        property stringList FPU: ["-mfpu=fpv4-sp-d16", "-mfpu=fpv4-sp-d16"]
        property string ld_script: "STM32F4XX.ld"

        cpp.defines:{
            var defs = base
            defs = defs.concat(["USE_BRD_F4_DISCOVERY","USE_STDPERIPH_DRIVER","STM32F4XX"])
            return defs
        }

        cpp.cxxFlags:{
            var flags = base
            if(cpp.compilerName.contains("g++"))
                flags = flags.concat(["-std=gnu++11"])
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }

        cpp.cFlags:{
            var flags = base
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU)
            flags = flags.concat(["-nostartfiles","-Wl,--gc-sections",
                                 "-lc", "-lgcc", "-specs=nano.specs","-u","-specs=rdimon.specs"])
            flags = flags.concat(["-Wl,-Map="+path+"/lst/project.map,--cref"])
            return flags
        }

        cpp.linkerScripts: [ "STM32F4XX.ld" ]

        cpp.includePaths:{
            var paths = base
            paths = paths.concat(["src","src/stm32f4xx",
                                  "src/stm32f4xx/drivers/inc",
                                  "src/CMSIS","src/utils",
                                  "src/board"])
            return paths
        }

    } // CppApplication

}




