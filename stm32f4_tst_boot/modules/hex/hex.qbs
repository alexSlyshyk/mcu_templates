import qbs
import qbs.ModUtils
import qbs.FileInfo
import qbs.TextFile
import qbs.Process

Module {
    id: hex
    additionalProductTypes: ["hex"]

    Depends{name:"cpp"}
    property string compilerPath : {return FileInfo.path(cpp.compilerPath)}

    Rule {
        id: obj_dump
        inputs: ["application"]
        multiplex: true
        Artifact {
            filePath: project.name+".hex"
            fileTags: ["hex"]
        }

        prepare: {
                var commands = []
                var cppPath = ModUtils.moduleProperty(product, "compilerPath") //product.compilerPath
                print(cppPath)
                var objcopy = cppPath + "/arm-none-eabi-objcopy"
                var objsize = cppPath + "/arm-none-eabi-size"
                var objdump = cppPath + "/arm-none-eabi-objdump"
                var elf_path = product.buildDirectory+"/"+project.name+".elf"

                var args = ["-O" , "ihex",elf_path,product.buildDirectory+"/"+project.name+".hex"]
                var cmd = new Command(objcopy,args);

                cmd.workingDirectory = cppPath
                cmd.description = "Processing " + project.name + ".hex";
                cmd.highlight = "filegen";

                commands = commands.concat([cmd])

                var args_bin = ["-O" , "binary",elf_path,product.buildDirectory+"/"+project.name+".bin"]
                var cmd_bin = new Command(objcopy,args_bin);

                cmd_bin.workingDirectory = cppPath
                cmd_bin.description = "Processing " + project.name + ".bin";
                cmd_bin.highlight = "filegen";

                commands = commands.concat([cmd_bin])

                var cmd_file = new JavaScriptCommand();
                cmd_file.description = "Creating listing file" ;
                cmd_file.objdump = objdump
                cmd_file.output = product.buildDirectory+"/"+project.name+".lst"
                cmd_file.elf = elf_path
                cmd_file.sourceCode = function() {
                    var pr = new Process();
                    pr.exec(objdump,["-dC",elf], true);
                    var text = pr.readStdOut();
                    var bash = new TextFile(output,TextFile.WriteOnly);
                    bash.write(text)
                    bash.close();
                };

            commands = commands.concat([cmd_file]);

                var args_size = ["-d" ,elf_path]
                var cmd_size = new Command(objsize,args_size);

                cmd_size.workingDirectory = cppPath
                cmd_size.description = " ----- View size ----- ";
                cmd_size.highlight = "filegen";
                commands = commands.concat([cmd_size]);

                return commands;
            }
    }
}
