/*******************************************************************************
*  file    : board.cpp
*  created : 21.09.2012
*  author  : Slyshyk Oleksiy (alex312@meta.ua)
*******************************************************************************/

#include "board.hpp"
#include "stm32f4xx_it.hpp"
#include "atomic_block.hpp"


void board::init()
{
    SysTick_Config (SystemCoreClock / 1000);  // 1000 Hz
    _BOARD::init_hw();
}


