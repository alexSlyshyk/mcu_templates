/*******************************************************************************
*  file    : board.hpp
*  created : 21.09.2012
*  author  : Slyshyk Oleksiy (alex312@meta.ua)
*******************************************************************************/

#ifndef BOARD_HPP
#define BOARD_HPP

// Дефайны для версий плат.
/*
USE_BRD_BS_02 - тестовая плата.
USE_BRD_BS_03_21 - первая "единая" плата с экраном и все на борту.
 */

#if   defined(USE_BRD_F4_DISCOVERY)
#include "brd_F4_discovery/brd_f4_discovery.hpp"
#define _BOARD brd_F4_discovery
#else
#error "Must define board revision !"
#endif

#include <stm32f4xx.h>
#include "stm32f4xx_conf.h"


class board
{
public:
    static void init();

    static inline void led_1_toggle(){_BOARD::led_1_toggle();}
    static inline void led_1_set   (){_BOARD::led_1_set()   ;}
    static inline void led_1_reset (){_BOARD::led_1_reset() ;}

    static inline void led_2_toggle(){_BOARD::led_2_toggle();}
    static inline void led_2_set   (){_BOARD::led_2_set()   ;}
    static inline void led_2_reset (){_BOARD::led_2_reset() ;}
};

#endif // BOARD_HPP
