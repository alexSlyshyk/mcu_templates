/*******************************************************************************
*  file    : brd_f4_discovery.hpp
*  created : 25.12.2013
*  author  : Slyshyk Oleksiy (alexSlyshyk@gmail.com)
*******************************************************************************/

#ifndef BRD_F4_DISCOVERY_HPP
#define BRD_F4_DISCOVERY_HPP

#include <stm32f4xx.h>
#include "stm32f4xx_conf.h"

class brd_F4_discovery
{
public:
    static void init_hw();

    static void led_1_toggle();
    static void led_1_set   ();
    static void led_1_reset ();

    static void led_2_toggle();
    static void led_2_set   ();
    static void led_2_reset ();
};

#endif // BRD_F4_DISCOVERY_HPP
