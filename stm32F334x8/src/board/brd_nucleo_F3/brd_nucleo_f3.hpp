/*******************************************************************************
*  file    :
*  created : 19.11.2015
*  author  :
*******************************************************************************/

#ifndef BRD_NUCLEO_F3_HPP
#define BRD_NUCLEO_F3_HPP

#include "stm32f30x_gpio.h"

#define LEDn                             4

#define LED1_PIN                         GPIO_Pin_5
#define LED1_GPIO_PORT                   GPIOA
#define LED1_GPIO_CLK                    RCC_AHBPeriph_GPIOA

#define LED2_PIN                         GPIO_Pin_8
#define LED2_GPIO_PORT                   GPIOB
#define LED2_GPIO_CLK                    RCC_AHBPeriph_GPIOB

#define LED3_PIN                         GPIO_Pin_9
#define LED3_GPIO_PORT                   GPIOB
#define LED3_GPIO_CLK                    RCC_AHBPeriph_GPIOB

#define LED4_PIN                         GPIO_Pin_7
#define LED4_GPIO_PORT                   GPIOB
#define LED4_GPIO_CLK                    RCC_AHBPeriph_GPIOB

class brd_nucleo_f3
{
public:
    static void init();

    static inline void led_1_toggle(){LED1_GPIO_PORT->ODR  ^= LED1_PIN;}
    static inline void led_1_set   (){LED1_GPIO_PORT->BSRR = LED1_PIN;}
    static inline void led_1_reset (){LED1_GPIO_PORT->BRR  = LED1_PIN;}

    static inline void led_2_toggle(){LED2_GPIO_PORT->ODR  ^= LED2_PIN;}
    static inline void led_2_set   (){}
    static inline void led_2_reset (){}
private:
    static void init_leds_gpio();
};

#endif // BRD_NUCLEO_F3_HPP
