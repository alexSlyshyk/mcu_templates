/*******************************************************************************
*  file    :
*  created : 08.11.2015
*  author  :
*******************************************************************************/

#ifndef BRD_F334_DISCO_HPP
#define BRD_F334_DISCO_HPP

#include "stm32f30x_gpio.h"

#define LEDn                             4

#define LED1_PIN                         GPIO_Pin_6
#define LED1_GPIO_PORT                   GPIOB
#define LED1_GPIO_CLK                    RCC_AHBPeriph_GPIOB

#define LED2_PIN                         GPIO_Pin_8
#define LED2_GPIO_PORT                   GPIOB
#define LED2_GPIO_CLK                    RCC_AHBPeriph_GPIOB

#define LED3_PIN                         GPIO_Pin_9
#define LED3_GPIO_PORT                   GPIOB
#define LED3_GPIO_CLK                    RCC_AHBPeriph_GPIOB

#define LED4_PIN                         GPIO_Pin_7
#define LED4_GPIO_PORT                   GPIOB
#define LED4_GPIO_CLK                    RCC_AHBPeriph_GPIOB



class brd_f334_disco
{
public:
    static void init();

    static inline void led_1_toggle(){LED1_GPIO_PORT->ODR  ^= LED1_PIN;}
    static inline void led_1_set   (){LED1_GPIO_PORT->BSRR = LED1_PIN;}
    static inline void led_1_reset (){LED1_GPIO_PORT->BRR  = LED1_PIN;}

    static inline void led_2_toggle(){LED2_GPIO_PORT->ODR  ^= LED2_PIN;}
    static inline void led_2_set   (){LED2_GPIO_PORT->BSRR = LED2_PIN;}
    static inline void led_2_reset (){LED2_GPIO_PORT->BRR  = LED2_PIN;}
private:
    static void init_leds_gpio();
};

#endif // BRD_F334_DISCO_HPP
