/*******************************************************************************
*  file    :
*  created : 08.11.2015
*  author  :
*******************************************************************************/

#include "brd_f334_disco.hpp"


void brd_f334_disco::init()
{
    init_leds_gpio();
}

void brd_f334_disco::init_leds_gpio()
{
    GPIO_InitTypeDef GPIO_InitStructure;
    RCC_AHBPeriphClockCmd(LED1_GPIO_CLK, ENABLE);
    RCC_AHBPeriphClockCmd(LED2_GPIO_CLK, ENABLE);
    RCC_AHBPeriphClockCmd(LED3_GPIO_CLK, ENABLE);
    RCC_AHBPeriphClockCmd(LED4_GPIO_CLK, ENABLE);
    /* Configure PE14 and PE15 in output pushpull mode */
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;

    GPIO_InitStructure.GPIO_Pin   = LED1_PIN;
    GPIO_Init(LED1_GPIO_PORT, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin   = LED2_PIN;
    GPIO_Init(LED2_GPIO_PORT, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin   = LED3_PIN;
    GPIO_Init(LED3_GPIO_PORT, &GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin   = LED4_PIN;
    GPIO_Init(LED4_GPIO_PORT, &GPIO_InitStructure);
}
