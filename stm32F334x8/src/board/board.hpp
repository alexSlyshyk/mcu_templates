/*******************************************************************************
*  file    : board.hpp
*  created : 21.09.2012
*  author  : Slyshyk Oleksiy (alex312@meta.ua)
*******************************************************************************/

#ifndef BOARD_HPP
#define BOARD_HPP

// Дефайны для версий плат.
/*
USE_BRD_BS_02 - тестовая плата.
USE_BRD_BS_03_21 - первая "единая" плата с экраном и все на борту.
 */

#if   defined(BRD_F334_DISCOVERY)
#include "board/brd_F3_discovery/brd_f334_disco.hpp"
#define _BOARD brd_f334_disco

#elif defined(BRD_NUCLEO_F3)
#include "board/brd_nucleo_F3/brd_nucleo_f3.hpp"
#define _BOARD brd_nucleo_f3

#else
#error "Must define board revision !"
#endif

#include <stm32f30x.h>
#include "stm32f30x_conf.h"


class board
{
public:
    static void init();

    static inline void led_1_toggle(){_BOARD::led_1_toggle();}
    static inline void led_1_set   (){_BOARD::led_1_set()   ;}
    static inline void led_1_reset (){_BOARD::led_1_reset() ;}

    static inline void led_2_toggle(){_BOARD::led_2_toggle();}
    static inline void led_2_set   (){_BOARD::led_2_set()   ;}
    static inline void led_2_reset (){_BOARD::led_2_reset() ;}
};

#endif // BOARD_HPP
