extern unsigned long _sidata;
extern unsigned long _sdata;
extern unsigned long _edata;
extern unsigned long _sbss;
extern unsigned long _ebss;
extern unsigned long _estack;
extern unsigned long __ctors_start__;
extern unsigned long __ctors_end__;

#define BootRAM (0xf1e0f85f)

void Reset_Handler  (void) __attribute__((__interrupt__));
void Default_Handler(void);
void __Init_Data    (void);

extern int  main(void);
extern void SystemInit(void);
extern void SystemCoreClockUpdate(void);

/*Interrupts headers*/
void NMI_Handler                    (void);
void HardFault_Handler              (void);
void MemManage_Handler              (void);
void BusFault_Handler               (void);
void UsageFault_Handler             (void);
void SVC_Handler                    (void);
void DebugMon_Handler               (void);
void PendSV_Handler                 (void);
void SysTick_Handler                (void);
void WWDG_IRQHandler                (void);
void PVD_IRQHandler                 (void);
void TAMP_STAMP_IRQHandler          (void);
void RTC_WKUP_IRQHandler            (void);
void FLASH_IRQHandler               (void);
void RCC_IRQHandler                 (void);
void EXTI0_IRQHandler               (void);
void EXTI1_IRQHandler               (void);
void EXTI2_TSC_IRQHandler           (void);
void EXTI3_IRQHandler               (void);
void EXTI4_IRQHandler               (void);
void DMA1_Channel1_IRQHandler       (void);
void DMA1_Channel2_IRQHandler       (void);
void DMA1_Channel3_IRQHandler       (void);
void DMA1_Channel4_IRQHandler       (void);
void DMA1_Channel5_IRQHandler       (void);
void DMA1_Channel6_IRQHandler       (void);
void DMA1_Channel7_IRQHandler       (void);
void ADC1_2_IRQHandler              (void);
void CAN_TX_IRQHandler              (void);
void CAN_RX0_IRQHandler             (void);
void CAN_RX1_IRQHandler             (void);
void CAN_SCE_IRQHandler             (void);
void EXTI9_5_IRQHandler             (void);
void TIM1_BRK_TIM15_IRQHandler      (void);
void TIM1_UP_TIM16_IRQHandler       (void);
void TIM1_TRG_COM_TIM17_IRQHandler  (void);
void TIM1_CC_IRQHandler             (void);
void TIM2_IRQHandler                (void);
void TIM3_IRQHandler                (void);
void I2C1_EV_IRQHandler             (void);
void I2C1_ER_IRQHandler             (void);
void SPI1_IRQHandler                (void);
void USART1_IRQHandler              (void);
void USART2_IRQHandler              (void);
void USART3_IRQHandler              (void);
void EXTI15_10_IRQHandler           (void);
void RTC_Alarm_IRQHandler           (void);
void TIM6_DAC1_IRQHandler           (void);
void TIM7_DAC2_IRQHandler           (void);
void COMP2_IRQHandler               (void);
void COMP4_6_IRQHandler             (void);
void HRTIM1_Master_IRQHandler       (void);
void HRTIM1_TIMA_IRQHandler         (void);
void HRTIM1_TIMB_IRQHandler         (void);
void HRTIM1_TIMC_IRQHandler         (void);
void HRTIM1_TIMD_IRQHandler         (void);
void HRTIM1_TIME_IRQHandler         (void);
void HRTIM1_FLT_IRQHandler          (void);
void FPU_IRQHandler                 (void);


/******************************************************************************
*                              Vector table                                   *
******************************************************************************/
typedef void( *const intfunc )( void );

__attribute__ ((used))
__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) = {
    (intfunc)((unsigned long)&_estack),
/*Interrupts*/
    Reset_Handler,
    NMI_Handler,
    HardFault_Handler,
    MemManage_Handler,
    BusFault_Handler,
    UsageFault_Handler,
    0, 0, 0, 0,
    SVC_Handler,
    DebugMon_Handler,
    0,
    PendSV_Handler,
    SysTick_Handler,
    WWDG_IRQHandler,
    PVD_IRQHandler,
    TAMP_STAMP_IRQHandler,
    RTC_WKUP_IRQHandler,
    FLASH_IRQHandler,
    RCC_IRQHandler,
    EXTI0_IRQHandler,
    EXTI1_IRQHandler,
    EXTI2_TSC_IRQHandler,
    EXTI3_IRQHandler,
    EXTI4_IRQHandler,
    DMA1_Channel1_IRQHandler,
    DMA1_Channel2_IRQHandler,
    DMA1_Channel3_IRQHandler,
    DMA1_Channel4_IRQHandler,
    DMA1_Channel5_IRQHandler,
    DMA1_Channel6_IRQHandler,
    DMA1_Channel7_IRQHandler,
    ADC1_2_IRQHandler,
    CAN_TX_IRQHandler,
    CAN_RX0_IRQHandler,
    CAN_RX1_IRQHandler,
    CAN_SCE_IRQHandler,
    EXTI9_5_IRQHandler,
    TIM1_BRK_TIM15_IRQHandler,
    TIM1_UP_TIM16_IRQHandler,
    TIM1_TRG_COM_TIM17_IRQHandler,
    TIM1_CC_IRQHandler,
    TIM2_IRQHandler,
    TIM3_IRQHandler,
    0,
    I2C1_EV_IRQHandler,
    I2C1_ER_IRQHandler,
    0, 0,
    SPI1_IRQHandler,
    0,
    USART1_IRQHandler,
    USART2_IRQHandler,
    USART3_IRQHandler,
    EXTI15_10_IRQHandler,
    RTC_Alarm_IRQHandler,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    TIM6_DAC1_IRQHandler,
    TIM7_DAC2_IRQHandler,
    0, 0, 0, 0, 0, 0, 0, 0,
    COMP2_IRQHandler,
    COMP4_6_IRQHandler,
    0,
    HRTIM1_Master_IRQHandler,
    HRTIM1_TIMA_IRQHandler,
    HRTIM1_TIMB_IRQHandler,
    HRTIM1_TIMC_IRQHandler,
    HRTIM1_TIMD_IRQHandler,
    HRTIM1_TIME_IRQHandler,
    HRTIM1_FLT_IRQHandler,
    0, 0, 0, 0, 0, 0, 0,
    FPU_IRQHandler
};

void Reset_Handler()
{
    __Init_Data();
    main();
}

void __Init_Data(void)
{
    unsigned long *pulSrc, *pulDest;
    pulSrc = &_sidata;
    for(pulDest = &_sdata; pulDest < &_edata; )
        *(pulDest++) = *(pulSrc++);
    for(pulDest = &_sbss; pulDest < &_ebss; )
        *(pulDest++) = 0;
    /* Init hardware before calling constructors */
    SystemInit();
    SystemCoreClockUpdate();
    /* Call constructors */
    unsigned long *ctors;
    for(ctors = &__ctors_start__; ctors < &__ctors_end__; )
        ((void(*)(void))(*ctors++))();
}

/*Weak aliases for each exception handler*/
#pragma weak NMI_Handler                    = Default_Handler 
#pragma weak HardFault_Handler              = Default_Handler 
#pragma weak MemManage_Handler              = Default_Handler 
#pragma weak BusFault_Handler               = Default_Handler 
#pragma weak UsageFault_Handler             = Default_Handler 
#pragma weak SVC_Handler                    = Default_Handler 
#pragma weak DebugMon_Handler               = Default_Handler 
#pragma weak PendSV_Handler                 = Default_Handler 
#pragma weak SysTick_Handler                = Default_Handler 
#pragma weak WWDG_IRQHandler                = Default_Handler 
#pragma weak PVD_IRQHandler                 = Default_Handler 
#pragma weak TAMP_STAMP_IRQHandler          = Default_Handler 
#pragma weak RTC_WKUP_IRQHandler            = Default_Handler 
#pragma weak FLASH_IRQHandler               = Default_Handler 
#pragma weak RCC_IRQHandler                 = Default_Handler 
#pragma weak EXTI0_IRQHandler               = Default_Handler 
#pragma weak EXTI1_IRQHandler               = Default_Handler 
#pragma weak EXTI2_TSC_IRQHandler           = Default_Handler 
#pragma weak EXTI3_IRQHandler               = Default_Handler 
#pragma weak EXTI4_IRQHandler               = Default_Handler 
#pragma weak DMA1_Channel1_IRQHandler       = Default_Handler 
#pragma weak DMA1_Channel2_IRQHandler       = Default_Handler 
#pragma weak DMA1_Channel3_IRQHandler       = Default_Handler 
#pragma weak DMA1_Channel4_IRQHandler       = Default_Handler 
#pragma weak DMA1_Channel5_IRQHandler       = Default_Handler 
#pragma weak DMA1_Channel6_IRQHandler       = Default_Handler 
#pragma weak DMA1_Channel7_IRQHandler       = Default_Handler 
#pragma weak ADC1_2_IRQHandler              = Default_Handler 
#pragma weak CAN_TX_IRQHandler              = Default_Handler 
#pragma weak CAN_RX0_IRQHandler             = Default_Handler 
#pragma weak CAN_RX1_IRQHandler             = Default_Handler 
#pragma weak CAN_SCE_IRQHandler             = Default_Handler 
#pragma weak EXTI9_5_IRQHandler             = Default_Handler 
#pragma weak TIM1_BRK_TIM15_IRQHandler      = Default_Handler 
#pragma weak TIM1_UP_TIM16_IRQHandler       = Default_Handler 
#pragma weak TIM1_TRG_COM_TIM17_IRQHandler  = Default_Handler 
#pragma weak TIM1_CC_IRQHandler             = Default_Handler 
#pragma weak TIM2_IRQHandler                = Default_Handler 
#pragma weak TIM3_IRQHandler                = Default_Handler 
#pragma weak I2C1_EV_IRQHandler             = Default_Handler 
#pragma weak I2C1_ER_IRQHandler             = Default_Handler 
#pragma weak SPI1_IRQHandler                = Default_Handler 
#pragma weak USART1_IRQHandler              = Default_Handler 
#pragma weak USART2_IRQHandler              = Default_Handler 
#pragma weak USART3_IRQHandler              = Default_Handler 
#pragma weak EXTI15_10_IRQHandler           = Default_Handler 
#pragma weak RTC_Alarm_IRQHandler           = Default_Handler 
#pragma weak TIM6_DAC1_IRQHandler           = Default_Handler 
#pragma weak TIM7_DAC2_IRQHandler           = Default_Handler 
#pragma weak COMP2_IRQHandler               = Default_Handler 
#pragma weak COMP4_6_IRQHandler             = Default_Handler 
#pragma weak HRTIM1_Master_IRQHandler       = Default_Handler 
#pragma weak HRTIM1_TIMA_IRQHandler         = Default_Handler 
#pragma weak HRTIM1_TIMB_IRQHandler         = Default_Handler 
#pragma weak HRTIM1_TIMC_IRQHandler         = Default_Handler 
#pragma weak HRTIM1_TIMD_IRQHandler         = Default_Handler 
#pragma weak HRTIM1_TIME_IRQHandler         = Default_Handler 
#pragma weak HRTIM1_FLT_IRQHandler          = Default_Handler 
#pragma weak FPU_IRQHandler                 = Default_Handler 

void Default_Handler(void){  for (;;);}
