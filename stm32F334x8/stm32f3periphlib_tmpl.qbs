import qbs
import qbs.FileInfo

Project {
    property string name : "stm32F3periphlib_tmpl"
    property string board: "nucleoF334" //"stm32F334discovery"
    property string mcu  : "STM32F334x8"
    qbsSearchPaths : ["~/projects/qbs_custom_modules/"]
    CppApplication {
        Depends {name:"hex"}
        Depends {name:"stm32f3periphlib" ; submodules:[project.mcu]}
        name:project.name
        targetName:{
            var tn = name
            if(project.with_bootloader)
                tn = tn + "_bt"
            tn = tn + ".elf"
            return tn
        }

        cpp.positionIndependentCode: false

        type: "application"

        Group{
            name:"main"
            prefix:"src/"
            files:["*.h","*.c","*.cpp","*.hpp"]
        }
        Group{
            name:"board"
            prefix:"src/board/"
            files:[
                "board.cpp",
                "board.hpp",
                "brd_F3_discovery/brd_f334_disco.cpp",
                "brd_F3_discovery/brd_f334_disco.hpp",
                "brd_nucleo_F3/brd_nucleo_f3.cpp",
                "brd_nucleo_F3/brd_nucleo_f3.hpp",
            ]
        }

        Group{
            name:"stm32f3"
            prefix:"src/stm32f334x8/"
            files:["*.h","*.c","*.cpp","*.hpp"]
        }
        Group{
            name:"utils"
            prefix:"src/utils/"
            files:["*.h","*.c","*.cpp","*.hpp"]
        }
        Group{
            name:"x_linkerscripts"
            files:["STM32F334R8_FLASH.ld"]
        }

        cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections", "-flto"]

        property stringList MCU: ["-mcpu=cortex-m4","-mthumb"]
        property stringList FPU: ["-mfpu=fpv4-sp-d16", "-mfloat-abi=hard"]
        property string ld_script: "STM32F4XX.ld"

        cpp.defines:{
            var defs = base
            if(project.board === "stm32F334discovery")
                defs = defs.concat(["BRD_F334_DISCOVERY"])
            else if(project.board == "nucleoF334")
                defs = defs.concat(["BRD_NUCLEO_F3"])

            defs = defs.concat(["HSE_VALUE=8000000"])
            return defs
        }

        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++11","-fno-exceptions","-fno-rtti", ])
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }

        cpp.cFlags:{
            var flags = base.concat("-std=gnu11")
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            flags = flags.concat(["-Wl,--gc-sections"])
            flags = flags.concat(["-flto"])
            var map_name = project.buildDirectory+"/"+product.name+".map";
            flags = flags.concat(["-Wl,-Map="+map_name+",--cref"])
            return flags
        }

        cpp.linkerScripts: [ /*"STM32F334x8.ld"*/ "STM32F334R8_FLASH.ld" ]

        cpp.includePaths:{
            var paths = base
            paths = paths.concat(["src",
                                  "src/stm32f334x8",
                                  "src/utils",
                                  "src/board"])
            return paths
        }

    } // CppApplication

}




