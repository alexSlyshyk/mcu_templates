import qbs

Project
{
    property string board : "stm32f4discovery"
    property string mcu: "STM32F401VC"
    property string name: "stm32f40x_template_cube"
    CppApplication {
       type: "application"
       consoleApplication: true

       name : project.name
       cpp.positionIndependentCode: false

        Group {
            name : "stm32f4xxlib"
            files : [
                "src/stm32f40xlib/Drivers/CMSIS/Device/ST/STM32F4xx/Include/stm32f4xx.h",
                "src/stm32f40xlib/Drivers/CMSIS/Device/ST/STM32F4xx/Include/system_stm32f4xx.h",
                "src/stm32f40xlib/Drivers/CMSIS/Include/core_cm4.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_adc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_adc_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_can.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_conf_template.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_cortex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_crc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_cryp.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_cryp_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dac.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dac_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dcmi.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_def.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dma.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dma2d.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_dma_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_eth.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_flash.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_flash_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_flash_ramfunc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_gpio.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_gpio_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_hash.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_hash_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_hcd.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_i2c.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_i2c_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_i2s.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_i2s_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_irda.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_iwdg.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_ltdc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_nand.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_nor.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pccard.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pcd.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pcd_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pwr.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_pwr_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rcc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rcc_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rng.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rtc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_rtc_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_sai.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_sd.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_sdram.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_smartcard.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_spi.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_sram.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_tim.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_tim_ex.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_uart.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_usart.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal_wwdg.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_fmc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_fsmc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_sdmmc.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_ll_usb.h",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_adc_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_can.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_crc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cryp_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dac_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dcmi.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma2d.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_eth.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hash_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_hcd.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2s_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_irda.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_iwdg.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_ltdc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nand.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_nor.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pccard.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rng.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rtc_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sai.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sd.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sdram.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_smartcard.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_sram.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_usart.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_wwdg.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fmc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_fsmc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_sdmmc.c",
                "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c",
            ]
        }

        Group {
            name: "stm32f4xx"
            files : [
                "src/stm32f40x/main.h",
                "src/stm32f40x/startups/startup_stm32f401xc.c",
                "src/stm32f40x/stm32f4xx_hal_conf.h",
                "src/stm32f40x/stm32f4xx_hal_msp.c",
                "src/stm32f40x/stm32f4xx_it.c",
                "src/stm32f40x/stm32f4xx_it.h",
                "src/stm32f40x/system_stm32f4xx.c",
            ]
        }

        Group {
            name : "app"
            files : [
                "src/main.cpp",
            ]
        }

        Group {
            name : "board"
            prefix : "src/board/"
            files: [
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery.c",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery.h",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery_accelerometer.c",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery_accelerometer.h",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery_gyroscope.c",
                "../stm32f40xlib/Drivers/BSP/STM32F401-Discovery/stm32f401_discovery_gyroscope.h",
            ]
        }


        Group {
            name : "x_other"
            files : [
                "README.md",
                "ldscripts/STM32F401CC_FLASH.ld",
                "ldscripts/STM32F407VG_FLASH.ld",
            ]
        }

        property stringList MCU: ["-mthumb","-mcpu=cortex-m4"]
        property stringList FPU: []

        cpp.defines:{
            var defs = base
            defs = defs.concat(["USE_STDPERIPH_DRIVER","USE_FULL_ASSERT"])
            if(project.mcu == "STM32F401VC")
                defs = defs.concat(["STM32F401xC"])

            return defs
        }

        cpp.linkerScripts : ["ldscripts/STM32F401CC_FLASH.ld"]
        cpp.commonCompilerFlags: {
            var flags = base;
            flags = flags.concat(["-g","-fdata-sections","-ffunction-sections","-flto","-fno-builtin","-nostdlib","-nostartfiles"])
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }
        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++11","-fno-exceptions","-fno-rtti","-fno-threadsafe-statics"])
            return flags
        }
        cpp.cFlags:{
            var flags = base
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            flags = flags.concat(["--specs=nano.specs","--specs=rdimon.specs"])
            flags = flags.concat(["-Wl,--gc-sections","-nostdlib", "-nostartfiles"])
            flags = flags.concat(["-lc"])
            flags = flags.concat(["-lgcc"])
            flags = flags.concat(["-lm"])
            flags = flags.concat(["-lrdimon"])
            flags = flags.concat(["-flto"])
            var map_name = project.buildDirectory+"/"+product.name+".map";
            flags = flags.concat(["-Wl,-Map="+map_name+",--cref"])

            return flags
        }

        cpp.includePaths : {
            var inc = base;
            inc = inc.concat(["src",
                              "src/stm32f40x",
                              "src/stm32f40xlib/Drivers/BSP/STM32F401-Discovery/",
                              "src/stm32f40xlib/Drivers/CMSIS/Include",
                              "src/stm32f40xlib/Drivers/STM32F4xx_HAL_Driver/Inc",
                              "src/stm32f40xlib/Drivers/CMSIS/Device/ST/STM32F4xx/Include",
                             ])

            return inc
        }

    }
}


