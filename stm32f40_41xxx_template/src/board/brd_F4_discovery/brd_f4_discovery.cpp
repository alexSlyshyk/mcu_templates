/*******************************************************************************
*  file    : brd_f4_discovery.cpp
*  created : 25.12.2013
*  author  : Slyshyk Oleksiy (alexSlyshyk@gmail.com)
*******************************************************************************/

#include "brd_f4_discovery.hpp"
#include "stm32f4_discovery.h"

void brd_F4_discovery::init_hw()
{
    STM_EVAL_LEDInit(LED4);
    STM_EVAL_LEDInit(LED3);
    STM_EVAL_LEDInit(LED5);
    STM_EVAL_LEDInit(LED6);
    STM_EVAL_PBInit(BUTTON_USER, BUTTON_MODE_GPIO);
}

void brd_F4_discovery::led_1_toggle()
{
    STM_EVAL_LEDToggle(LED4);
}

void brd_F4_discovery::led_1_set()
{
    STM_EVAL_LEDOn(LED4);
}

void brd_F4_discovery::led_1_reset()
{
    STM_EVAL_LEDOff(LED4);
}

void brd_F4_discovery::led_2_toggle()
{
    STM_EVAL_LEDToggle(LED3);
}

void brd_F4_discovery::led_2_set()
{
    STM_EVAL_LEDOn(LED3);
}

void brd_F4_discovery::led_2_reset()
{
    STM_EVAL_LEDOff(LED3);
}
