
#include <cstdint>
#include "board.hpp"

int main()
{
    board::init();
    while(1)
        {

        }
    return 0;
}


#ifdef USE_FULL_ASSERT
/* ******************************************************************************
* Function Name  : assert_failed
* Description    : Reports the name of the source file and the source line number
*                  where the assert_param error has occurred.
* Input          : - file: pointer to the source file name
*                  - line: assert_param error line source number
* Output         : None
* Return         : None
*******************************************************************************/
extern "C" void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    //fmt_out("ASSERT in %s at %i\r\n",(const char*)file, line );
}
#endif
