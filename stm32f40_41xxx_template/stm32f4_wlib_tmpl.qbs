import qbs
import qbs.FileInfo

Project {
    property string name : "stm32F4_tst_boot"
    property string board: "stm32F4discovery"
    property string mcu  : "STM32F40_41xxx"
    qbsSearchPaths : ["~/projects/qbs_custom_modules/"]
    CppApplication {
        Depends {name:"hex"}
        Depends {name:"stm32f4periphlib" ; submodules:[project.mcu]}
        name:project.name
        cpp.executableSuffix: ".elf"
        cpp.positionIndependentCode: false

        type: "application"

        Group{
            name:"main"
            prefix:"src/"
            files:["*.h","*.c","*.cpp","*.hpp"]
        }
        Group{
            name:"board"
            prefix:"src/board/"
            files:["board.cpp","board.hpp",
                "brd_F4_discovery/brd_f4_discovery.hpp",
                "brd_F4_discovery/brd_f4_discovery.cpp",
                "brd_F4_discovery/stm32f4_discovery.h",
                "brd_F4_discovery/stm32f4_discovery.c"]
        }

        Group{
            name:"stm32f4"
            prefix:"src/stm32f4xx/"
            files:["*.h","*.c","*.cpp","*.hpp"]
        }
        Group{
            name:"utils"
            prefix:"src/utils/"
            files:["*.h","*.c","*.cpp","*.hpp"]
        }

        cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections", "-flto"]

        property stringList MCU: ["-mcpu=cortex-m4","-mthumb"]
        property stringList FPU: ["-mfpu=fpv4-sp-d16", "-mfloat-abi=hard"]
        property string ld_script: "STM32F4XX.ld"

        cpp.defines:{
            var defs = base
            defs = defs.concat(["USE_BRD_F4_DISCOVERY"])
            defs = defs.concat(["STM32F4XX"])
            defs = defs.concat(["HSE_VALUE=8000000"])
            return defs
        }

        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++11","-fno-exceptions","-fno-rtti", ])
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }

        cpp.cFlags:{
            var flags = base.concat("-std=gnu11")
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            flags = flags.concat(["-Wl,--gc-sections",
                                 "-lc", "-lgcc", "-specs=nano.specs","-u","-specs=rdimon.specs"])
            flags = flags.concat(["-flto"])
            var map_name = project.buildDirectory+"/"+product.name+".map";
            flags = flags.concat(["-Wl,-Map="+map_name+",--cref"])
            return flags
        }

        cpp.linkerScripts: [ "STM32F4XX.ld" ]

        cpp.includePaths:{
            var paths = base
            paths = paths.concat(["src",
                                  "src/stm32f4xx",
                                  "src/utils",
                                  "src/board"])
            return paths
        }

    } // CppApplication

}




