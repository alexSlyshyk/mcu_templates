#! /bin/bash

source set_vars.sh


echo "Make HEX" ../exe/$TARGET_NAME.hex
$GNU_TOOLS_DIR/bin/arm-none-eabi-objcopy -O ihex   ../build/qtc_$QT_SDK-release/$TARGET_NAME.qtc_$QT_SDK/$TARGET_NAME.elf   ../exe/$TARGET_NAME.hex
echo "Make BIN" ../exe/$TARGET_NAME.bin
$GNU_TOOLS_DIR/bin/arm-none-eabi-objcopy -O binary ../build/qtc_$QT_SDK-release/$TARGET_NAME.qtc_$QT_SDK/$TARGET_NAME.elf   ../exe/$TARGET_NAME.bin
echo "Make LSS" ../exe/$TARGET_NAME.lss
$GNU_TOOLS_DIR/bin/arm-none-eabi-objdump -dC       ../build/qtc_$QT_SDK-release/$TARGET_NAME.qtc_$QT_SDK/$TARGET_NAME.elf > ../exe/$TARGET_NAME.lss

#echo "MUX Deploy to RPi"
#sshpass -p 'pi' scp ../exe/$TARGET_NAME.hex  pi@192.168.2.181:/home/pi/exe/$TARGET_NAME.hex_


