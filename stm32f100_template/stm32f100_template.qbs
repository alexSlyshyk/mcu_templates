import qbs

Project
{
    property string board : "stm32f100discovery"
    property string mcu: "stm32f100C8"
    property string name: "stm32f100_template"
    CppApplication {
       type: "application"
       consoleApplication: true

       name : project.name
       cpp.executableSuffix: ".elf"
       cpp.positionIndependentCode: false

        Group {
            name : "stm32f0_lib"
            files : [
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/misc.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_adc.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_bkp.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_can.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_cec.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_crc.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_dac.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_dbgmcu.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_dma.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_exti.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_flash.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_fsmc.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_gpio.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_i2c.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_iwdg.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_pwr.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_rcc.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_rtc.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_sdio.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_spi.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_tim.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_usart.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc/stm32f10x_wwdg.h",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/misc.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_adc.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_bkp.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_can.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_cec.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_crc.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_dac.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_dbgmcu.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_dma.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_exti.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_flash.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_fsmc.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_gpio.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_i2c.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_iwdg.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_pwr.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_rcc.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_rtc.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_sdio.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_spi.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_tim.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_usart.c",
                "src/stm32f100lib/STM32F10x_StdPeriph_Driver/src/stm32f10x_wwdg.c",
            ]
        }

        Group {
            name: "mcu_concrete"
            files : [
                "src/startup_stm32f100_ld_vl.c",
                "src/startup_stm32f100_md_vl.c",
                "src/stm32f10x_conf.h",
                "src/stm32f10x_it.cpp",
                "src/stm32f10x_it.h",
                "src/system_stm32f10x.c",
            ]
        }

        Group {
            name : "app"
            files : ["src/main.cpp"]
        }

        Group {
            name : "x_other"
            files : [
                "README.md",
                "ldscripts/stm32_ld_vl_flash.ld",
                "ldscripts/stm32_md_vl_flash.ld",
            ]
        }

        property stringList MCU: ["-mthumb","-mcpu=cortex-m3"]
        property stringList FPU: []

        cpp.defines:{
            var defs = base
            defs = defs.concat(["USE_STDPERIPH_DRIVER","USE_FULL_ASSERT"])
            if(project.mcu == "stm32f100C6")
                defs = defs.concat(["STM32F10X_LD_VL"])
            if(project.mcu == "stm32f100C8")
                defs = defs.concat(["STM32F10X_MD_VL"])
            return defs
        }

        cpp.linkerScripts : {
            var scipts = base
            if(project.mcu == "stm32f100C6")
                scipts = scipts.concat(["ldscripts/stm32_ld_vl_flash.ld"])
            if(project.mcu == "stm32f100C8")
                scipts = scipts.concat(["ldscripts/stm32_md_vl_flash.ld"])

            return scipts
        }

        cpp.commonCompilerFlags: {
            var flags = base;
            flags = flags.concat(["-fdata-sections","-ffunction-sections","-flto","-fno-builtin","-nostdlib"])
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            return flags
        }
        cpp.cxxFlags:{
            var flags = base
            flags = flags.concat(["-std=gnu++11","-fno-exceptions","-fno-rtti"])
            return flags
        }
        cpp.cFlags:{
            var flags = base
            return flags
        }

        cpp.linkerFlags:{
            var flags = base
            flags = flags.concat(MCU)
            flags = flags.concat(FPU)
            flags = flags.concat(["--specs=nano.specs","--specs=rdimon.specs"])
            flags = flags.concat(["-Wl,--gc-sections","-lc", "-lgcc", "-lm", "-lrdimon"])
            flags = flags.concat(["-flto"])
            var map_name = project.buildDirectory+"/"+product.name+".map";
            flags = flags.concat(["-Wl,-Map="+map_name+",--cref"])

            return flags
        }

        cpp.includePaths : {
            var inc = base;
            inc = inc.concat(["src",
                              "src/stm32f100lib/CMSIS/CM3/CoreSupport",
                              "src/stm32f100lib/CMSIS/CM3/DeviceSupport/ST/STM32F10x",
                              "src/stm32f100lib/STM32F10x_StdPeriph_Driver/inc"])

            return inc
        }

    }
}


